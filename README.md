# DSCplot

by Nils Trapp (c) 2019-2022 -  https://gitlab.ethz.ch/trappn

## About

Simple Qt5 application for fast Linkam DSC data display:
DSCPlot is a free plotting toolkit for DSC curves produced by
LINK ((c) Linkam Scientific). The program comes with a simple
graphical interface. Specifically, this was written for a Linkam DSC600
Temperature/DSC/Video stage (https://www.linkam.co.uk/dsc600), but should work with data from other instruments.

DSCPlot includes icons downloaded from https://icons8.com
which are redistributed within the terms of their license agreement.
A copy of this agreement is distributed with DSCPlot.<br>
DSCplot itself is under MIT license, because I basically don't care much what you do with it (but see notes below if you're a crook).

## Installers

Windows Installer (64bit, Windows 7 or later):
https://gitlab.ethz.ch/trappn/dscplot/-/tree/main/installers/dscplot_win_1.0.1.exe<br>

## Usage / Features

Export data from LINK in .txt / .csv format, and load the file in DSCPlot.

- automatically converts & cleans up input files
- drops all data except Index, Time, Temp, DSC & TASC
- more meaningful/readable column headers
- gets rid of 1000-separators, reformats data to 3 decimal places
- insert semi-colons as separators (= Excel legacy)
- converted files are usable "as-is" in Excel and Origin
- detects if data points were compressed or not
- classic T vs. heat flow plot or time plot
- plots TASC data when present
- economy class data editing (i.e. you can select/delete data in a table)
- multiple editing / printing / conversion / display functions

## Screenshots

![GUI overview image](/images/gallery1.png "DSCplot GUI window")

## Current Status

Work in progress. Basic features and customization work. If you click on a menu item and only get the "About" textbox, that feature is not implemented yet. More will be added strictly on a need-to-have basis, but feel free to get in touch and contribute.
Also: I asked for the definition of Linkam's raw data format, and did not get it. So binary files created by the instrument cannot be read, you have to export them in text format first.

## Notes / Legal

No responsibility whatsoever taken for any damage, for whatever cause or reason,
through use or misuse of this software by anybody and their mum. I am not a software company; if you dislike this program
or it doesn't do what you want, I might help out if you ask nicely - if I have time and am in a good mood. If you find
anything in the code owned by you that I'm not legally allowed to use, let me know and I'll take it down or remove it from the program.
I'll not pursue anyone cloning/stealing/abusing/selling this code without giving me credit, but if you do, know you are a terrible person
and shall rot in hell. Take it from me that, at least in science, Karma is real. Complaints and rants will be ignored, I just can't be bothered.
