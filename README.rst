============
 DSCPlot
============

Authors
=======
By Nils Trapp (c) 2019-2020 -  https://gitlab.ethz.ch/trappn

About
=====

Simple Qt5 application for fast Linkam DSC data display:
DSCPlot is a free plotting toolkit for DSC curves produced by
LINK ((c) Linkam Scientific). The program comes with a simple
graphical interface. This was written for a Linkam DSC600
Temperature/DSC/Video stage.

DSCPlot includes icons downloaded from https://icons8.com
which are redistributed within the terms of their license agreement.
A copy of this agreement is distributed with DSCPlot.

Usage
=====

Export data from LINK in .txt / .csv format, and load the file in DSCPlot.

- automatically converts & cleans up input files
- drops all data except Index, Time, Temp, DSC & TASC
- more meaningful/readable column headers
- gets rid of 1000-separators, reformats data to 3 decimal places
- insert semi-colons as separators (= Excel legacy)
- converted files are usable "as-is" in Excel and Origin
- detects if data points were compressed or not
- classic T vs. heat flow plot or time plot
- plots TASC data when present
- multiple editing / printing / conversion / display functions

Contributing
============

Submit bugs and patches to the `public git repository <https://gitlab.ethz.ch/trappn>`_.

Notes
=====

No responsibility taken for any damage, for whatever cause or reason,
through use of this software.