from PyQt5 import QtCore as qtc
from PyQt5 import QtWidgets as qtw

class DataTableForm(qtw.QWidget):
    """ Widget for display & edit of numerical raw data """

    model = None
    removed = qtc.pyqtSignal()
    restored = qtc.pyqtSignal()

    def __init__(self):
        super().__init__()
        self.setLayout(qtw.QVBoxLayout())
        self.table = qtw.QTableView()
        self.table.setSortingEnabled(True)
        # don't show inbuilt indeces (we have indexed data):
        self.table.verticalHeader().setVisible(False);
        # fit columns to widget width:
        self.table.horizontalHeader().setSectionResizeMode(qtw.QHeaderView.Stretch)
        # change selection behavior to full line selection instead of single cells
        self.table.setSelectionBehavior(qtw.QAbstractItemView.SelectRows)
        # make cells non-editable:
        self.table.setEditTriggers(qtw.QAbstractItemView.NoEditTriggers)
        self.buttons = qtw.QWidget()
        self.buttons.setLayout(qtw.QHBoxLayout())
        self.delbutton = qtw.QPushButton(
            'Delete Selection')
        self.delbutton.clicked.connect(self.remove_rows)
        self.restorebutton = qtw.QPushButton(
            'Restore All')
        self.restorebutton.clicked.connect(self.restore_rows)
        self.buttons.layout().addWidget(self.delbutton)
        self.buttons.layout().addWidget(self.restorebutton)
        self.layout().addWidget(self.table)
        self.layout().addWidget(self.buttons)
        # disable all buttons initially:
        for button in self.findChildren(qtw.QPushButton):
            button.setEnabled(False)
    
    def remove_rows(self):
        selected = self.table.selectedIndexes()
        num_rows = len(set(index.row() for index in selected))
        if selected:
            self.model.removeRows(selected[0].row(), num_rows, None)
            self.removed.emit()

    def restore_rows(self):
        self.restored.emit()
        

class CsvTableModel(qtc.QAbstractTableModel):
    """The model for a CSV table."""

    def __init__(self, data, headers):
        super().__init__()
        self._headers = headers
        self._data = data

    def rowCount(self, parent):
        return len(self._data)

    def columnCount(self, parent):
        # since we are adding a virtual column, we need one more column
        return len(self._headers)+1
        
    def tableDataAsList(self):
        dlist = []
        for row in range(len(self._data)):
            drow = []
            for col in range(len(self._headers)):
                drow.append(self._data[row][col])
            dlist.append(drow)
        return dlist

    def data(self, index, role=qtc.Qt.DisplayRole):
        # including hack for Kelvin calculated temps at pos. 3:
        if role in (qtc.Qt.DisplayRole, qtc.Qt.EditRole) and index.column()==3:
            # actual Kelvin conversion:
            return '{:.3f}'.format(float(self.data(self.index(index.row(), index.column()-1))) + 273.15)
        elif role in (qtc.Qt.DisplayRole, qtc.Qt.EditRole) and index.column()>3:
            # shift left by one after Kelvin column:
            index = self.index(index.row(), index.column()-1)
            return self._data[index.row()][index.column()]
        elif role in (qtc.Qt.DisplayRole, qtc.Qt.EditRole):
            return self._data[index.row()][index.column()]

    def headerData(self, section, orientation, role):
        # for column headers as used here, return requested data,
        # including hack for Kelvin calculated temps at pos. 3:
        if section==3 and orientation == qtc.Qt.Horizontal and role == qtc.Qt.DisplayRole:
            return 'T[K]'
        elif section>3 and orientation == qtc.Qt.Horizontal and role == qtc.Qt.DisplayRole:
            # shift all headers after virtual column left:
            section -= 1
            return self._headers[section]
        elif orientation == qtc.Qt.Horizontal and role == qtc.Qt.DisplayRole:
            return self._headers[section]
        # fall back to parent class logic for everything else:
        else:
            return super().headerData(section, orientation, role)

    def sort(self, column, order):
        self.layoutAboutToBeChanged.emit()
        # needs to be emitted before a sort, take into account we have a virtual column at 3,
        # need float conversion to get proper sorting (negatives and None would screw it up)
        if column > 3:
            self._data.sort(key=lambda x: float(x[column-1] or 0))
        else:
            self._data.sort(key=lambda x: float(x[column] or 0))
        if order == qtc.Qt.DescendingOrder:
            self._data.reverse()
        self.layoutChanged.emit()

    def flags(self, index):
        return super().flags(index) | qtc.Qt.ItemIsEditable

    def removeRows(self, position, rows, parent):
        self.beginRemoveRows(
            parent or qtc.QModelIndex(),
            position,
            position + rows - 1
        )
        for i in range(rows):
            del(self._data[position])
        self.endRemoveRows()