from os import path
import sys

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
from PyQt5 import QtChart as qtch

from .auxiliary import getSettings
from math import ceil, floor

class DscVsTimeView(qtw.QWidget):
    """ Display Link-Style plot Temp/DSC/TASC vs time """

    tascPresent = qtc.pyqtSignal()
    tascNotPresent = qtc.pyqtSignal()
    dscOnly = False
    updated = qtc.pyqtSignal()
    dscShown = True
    tascShown = True
    splinesShown = True
    chart_title = "DSC/T <i>vs.</i> t Plot"

    def __init__(self):
        super().__init__()
        # retrieve settings:
        getSettings(self)
        ### Make icons available:
        if getattr(sys, 'frozen', False):
            # compiled with PyInstaller:
            self.directory = sys._MEIPASS
        else:
            # run as python script:
            self.directory = path.dirname(__file__)
        # layout:
        self.setLayout(qtw.QHBoxLayout())
        self.view = qtch.QChartView()
        self.toolLayout = qtw.QBoxLayout(qtw.QBoxLayout.TopToBottom)
        self.toolbar = qtw.QToolBar(
            'Graph',
            orientation=qtc.Qt.Vertical)
 
        self.contentsLayout = qtw.QBoxLayout(qtw.QBoxLayout.TopToBottom)
        self.contentsLayout.addWidget(self.view)
        self.layout().addLayout(self.toolLayout)
        self.layout().addLayout(self.contentsLayout)
        self.chart = qtch.QChart(title=self.chart_title)
        self.chart.setTitleFont(self.titleFont)
        self.chart.setTitleBrush(qtg.QBrush(self.axisColor))
        self.chart.setBackgroundBrush(qtg.QBrush(self.backgroundColor))
        self.chart.layout().setContentsMargins(0, 0, 0, 0)
        self.chart.setBackgroundRoundness(0)
        self.view.setChart(self.chart)
        # toolbar:
        #self.toolbar_tasc = self.toolbar.addAction('Toggle TASC')
        #self.toolbar_tasc.setIcon(qtg.QIcon(path.join(self.directory, 'icons', 'tasc-toggle-50.png')))
        #self.toolbar_tasc.triggered.connect(self.toggleTasc)

        self.seriesT = qtch.QSplineSeries(name="Temperature")
        self.seriesTRough = qtch.QLineSeries(name="Temperature")
        self.seriesDsc = qtch.QSplineSeries(name="DSC")
        self.seriesTasc = qtch.QSplineSeries(name="TASC")
        self.seriesDscRough = qtch.QLineSeries(name="DSC")
        self.seriesTascRough = qtch.QLineSeries(name="TASC")
        self.chart.addSeries(self.seriesT)
        self.chart.addSeries(self.seriesTRough)
        self.chart.addSeries(self.seriesDsc)
        self.chart.addSeries(self.seriesDscRough)
        self.chart.addSeries(self.seriesTasc)
        self.chart.addSeries(self.seriesTascRough)
        self.x_axis = qtch.QCategoryAxis(
            labelsPosition=qtch.QCategoryAxis.AxisLabelsPositionOnValue,
            startValue=0.0)
        self.y_axisT = qtch.QCategoryAxis(
            labelsPosition=qtch.QCategoryAxis.AxisLabelsPositionOnValue,
            startValue=0.0)
        self.y_axisDsc = qtch.QCategoryAxis(
            labelsPosition=qtch.QCategoryAxis.AxisLabelsPositionOnValue,
            startValue=0.0,
            gridVisible=False)
        self.y_axisTasc = qtch.QCategoryAxis(
                labelsPosition=qtch.QCategoryAxis.AxisLabelsPositionOnValue,
                startValue=0.0,
                gridVisible=False)
        self.chart.setAxisX(self.x_axis, self.seriesT)
        self.chart.setAxisX(self.x_axis, self.seriesTRough)
        self.chart.setAxisY(self.y_axisT, self.seriesT)
        self.chart.setAxisY(self.y_axisT, self.seriesTRough)
        self.chart.setAxisX(self.x_axis, self.seriesDsc)
        self.chart.setAxisX(self.x_axis, self.seriesDscRough)
        self.chart.addAxis(self.y_axisDsc, qtc.Qt.AlignRight)
        self.seriesDsc.attachAxis(self.y_axisDsc)
        self.seriesDscRough.attachAxis(self.y_axisDsc)
        self.chart.setAxisX(self.x_axis, self.seriesTasc)
        self.chart.setAxisX(self.x_axis, self.seriesTascRough)
        self.chart.addAxis(self.y_axisTasc, qtc.Qt.AlignRight)
        self.seriesTasc.attachAxis(self.y_axisTasc)
        self.seriesTascRough.attachAxis(self.y_axisTasc)
        self.chart.legend().setVisible(False)
        self.view.setRenderHint(qtg.QPainter.Antialiasing)

    def restyle(self):
        getSettings(self)
        self.chart.setTitleFont(self.titleFont)
        self.chart.setTitleBrush(qtg.QBrush(self.axisColor))
        self.chart.setBackgroundBrush(qtg.QBrush(self.backgroundColor))
        self.chart.update()

    def refresh(self, data):
        # retrieve updated settings:
        getSettings(self)
        # load toolbar:
        self.toolLayout.addWidget(self.toolbar)
        # (re-)apply visual settings:
        self.seriesT.setColor(self.tempColor)
        self.seriesDsc.setColor(self.dscColor)
        self.seriesTasc.setColor(self.tascColor)
        self.seriesTRough.setColor(self.tempColor)
        self.seriesDscRough.setColor(self.dscColor)
        self.seriesTascRough.setColor(self.tascColor)
        self.chart.legend().setFont(self.axisFont)
        self.chart.setTitleFont(self.titleFont)
        self.chart.setTitleBrush(qtg.QBrush(self.axisColor))
        self.chart.setBackgroundBrush(qtg.QBrush(self.backgroundColor))
        self.x_axis.setLabelsFont(self.axisFont)
        self.x_axis.setLabelsBrush(qtg.QBrush(self.axisColor))
        self.y_axisT.setTitleFont(self.axisFont)
        self.y_axisT.setLabelsFont(self.axisFont)
        self.y_axisDsc.setLabelsFont(self.axisFont)
        self.y_axisTasc.setLabelsFont(self.axisFont)
        self.x_axis.setTitleFont(self.axisFont)
        self.x_axis.setTitleBrush(qtg.QBrush(self.axisColor))

        self.data = data
        ncol = len(data[0])
        newDataT = []
        newDataDsc = []
        newDataTasc = []
        templst = []

        if ncol==6:
            for item in self.data:
                try:
                    templst.append(float(item[2]))
                except:
                    pass
            maxTemp = max(templst) + 5
            minTemp = min(templst) - 5
            templst.clear()
            for item in self.data:
                try:
                    templst.append(float(item[1]))
                except:
                    pass            
            maxTime = max(templst)
            minTime = min(templst)
            templst.clear()
            for item in self.data:
                try:
                    templst.append(float(item[3]))
                except:
                    pass              
            maxDsc = max(templst) + 500
            minDsc = min(templst) - 500
            templst.clear()
            maxTasc = max([ (0.0) if x[5] in (None,'') else float(x[5]) for x in data])
            minTasc = min([ (0.0) if x[5] in (None,'') else float(x[5]) for x in data])
            for i in data:
                try:
                    newDataT.append(qtc.QPointF(float(i[1]), float(i[2]))) # t,T   
                except ValueError:
                    pass
                try:
                    newDataDsc.append(qtc.QPointF(float(i[1]), float(i[3]))) # t,DSC   
                except ValueError:
                    pass
                try:
                    newDataTasc.append(qtc.QPointF(float(i[4]), float(i[5]))) # xTASC, yTASC   
                except ValueError:
                    pass
        else:
            for item in self.data:
                try:
                    templst.append(float(item[3]))
                except:
                    pass
            maxTemp = max(templst) + 5
            minTemp = min(templst) - 5
            templst.clear()
            for item in self.data:
                try:
                    templst.append(float(item[2]))
                except:
                    pass            
            maxTime = max(templst)
            minTime = min(templst)
            templst.clear()
            for item in self.data:
                try:
                    templst.append(float(item[4]))
                except:
                    pass              
            maxDsc = max(templst) + 500
            minDsc = min(templst) - 500
            templst.clear()
            maxTasc = max([ (0.0) if x[6] in (None,'') else float(x[6]) for x in data])
            minTasc = min([ (0.0) if x[6] in (None,'') else float(x[6]) for x in data])
            for i in data:
                try:
                    newDataT.append(qtc.QPointF(float(i[2]), float(i[3]))) # t,T   
                except ValueError:
                    pass
                try:
                    newDataDsc.append(qtc.QPointF(float(i[2]), float(i[4]))) # t,DSC   
                except ValueError:
                    pass
                try:
                    newDataTasc.append(qtc.QPointF(float(i[5]), float(i[6]))) # xTASC, yTASC   
                except ValueError:
                    pass
        # TASC detection/toggling: 
        if len(newDataTasc) > 0:
            self.tascPresent.emit()
            self.dscOnly = False
            if self.tascShown == True:
                self.seriesTasc.setVisible(True)
                self.y_axisTasc.setVisible(True)
        else:
            self.tascNotPresent.emit()
            self.seriesTasc.setVisible(False)
            self.y_axisTasc.setVisible(False)
            self.tascShown = False
            self.dscOnly = True
        # format & attach axis to both chart & series, also °C/K toggling:
        self.chart.legend().setVisible(True)
        self.x_axis.setTitleText('t / s')
        if self.kelvin:
            maxTemp += 273.15
            minTemp += 273.15
            newDataT[:] = [ qtc.QPointF(i.x(),i.y()+273.15) for i in newDataT ]
            self.y_axisT.setTitleText('T / K')
        else:
            self.y_axisT.setTitleText('T / °C')
        self.y_axisT.setLinePenColor(self.seriesT.pen().color())
        self.y_axisT.setLabelsColor(self.seriesT.pen().color())
        self.y_axisT.setTitleBrush(qtg.QBrush(self.seriesT.pen().color()))
        self.y_axisDsc.setLinePenColor(self.seriesDsc.pen().color())
        self.y_axisDsc.setLabelsColor(self.seriesDsc.pen().color())
        self.y_axisTasc.setLinePenColor(self.seriesTasc.pen().color())
        self.y_axisTasc.setLabelsColor(self.seriesTasc.pen().color())
        try:
            self.formatAxis(self.y_axisT,
                        minTemp-max(abs(minTemp),abs(maxTemp))*0.05,
                        maxTemp+max(abs(minTemp),abs(maxTemp))*0.05,
                        round(abs(maxTemp-minTemp)/8,-1))
        except:
            pass
        try:
            self.formatAxis(self.x_axis,
                        minTime,
                        maxTime,
                        round(maxTime/10,-2))
        except:
            pass
        try:
            self.formatAxis(self.y_axisDsc,
                        minDsc-max(abs(minDsc),abs(maxDsc))*0.05,
                        maxDsc+max(abs(minDsc),abs(maxDsc))*0.05,
                        round(abs(maxDsc-minDsc)/6,-3))
        except:
            pass
        try:
            self.formatAxis(self.y_axisTasc,
                        minTasc-max(abs(minTasc),abs(maxTasc))*0.05,
                        maxTasc+max(abs(minTasc),abs(maxTasc))*0.05,
                        round(abs(maxTasc-minTasc)/6,1))
        except:
            pass
        # update data in series:
        self.seriesT.replace(newDataT)
        self.seriesTRough.replace(newDataT)
        self.seriesDsc.replace(newDataDsc)
        self.seriesDscRough.replace(newDataDsc)
        self.seriesTasc.replace(newDataTasc)
        self.seriesTascRough.replace(newDataTasc)
        # make rough curves invisible initially:
        self.seriesTRough.setVisible(False)
        self.seriesDscRough.setVisible(False)
        self.seriesTascRough.setVisible(False)
        # call switched functions:
        self.setCurves()
        
    def toggleTasc(self):
        self.tascShown = not self.tascShown
        self.setCurves()
   
    def toggleDsc(self):
        self.dscShown = not self.dscShown
        self.setCurves()

    def setCurves(self):
        # DSC shown
        if (self.dscShown and not self.tascShown) or (self.dscShown and self.dscOnly):
            self.x_axis.setVisible(True)
            self.y_axisDsc.setVisible(True)
            self.y_axisTasc.setVisible(False)
            self.chart_title = "DSC/T <i>vs.</i> t Plot"
            self.seriesTasc.setVisible(False)
            self.seriesTascRough.setVisible(False)
            if self.splinesShown == True:
                self.seriesDsc.setVisible(True)
                self.seriesDscRough.setVisible(False)
            else:
                self.seriesDsc.setVisible(False)
                self.seriesDscRough.setVisible(True)
        # TASC shown:
        elif self.tascShown and not self.dscShown and not self.dscOnly:
            self.y_axisDsc.setVisible(False)
            self.y_axisTasc.setVisible(True)
            self.chart_title = "TASC/T <i>vs.</i> t Plot"
            self.seriesDsc.setVisible(False)
            self.seriesDscRough.setVisible(False)
            if self.splinesShown == True:
                self.seriesTasc.setVisible(True)
                self.seriesTascRough.setVisible(False)
            else:
                self.seriesTasc.setVisible(False)
                self.seriesTascRough.setVisible(True)
        # DSC & TASC shown:
        elif self.dscShown and self.tascShown:
            self.y_axisDsc.setVisible(True)
            self.y_axisTasc.setVisible(True)
            self.chart_title = "DSC/TASC/T <i>vs.</i> t Plot"
            if self.splinesShown == True:
                self.seriesDsc.setVisible(True)
                self.seriesDscRough.setVisible(False)
                self.seriesTasc.setVisible(True)
                self.seriesTascRough.setVisible(False)
            else:
                self.seriesDsc.setVisible(False)
                self.seriesDscRough.setVisible(True)
                self.seriesTasc.setVisible(False)
                self.seriesTascRough.setVisible(True)
        #nothing shown:
        else:
            self.seriesDsc.setVisible(False)
            self.seriesDscRough.setVisible(False)
            self.seriesTasc.setVisible(False)
            self.seriesTascRough.setVisible(False)
            self.y_axisDsc.setVisible(False)
            self.y_axisTasc.setVisible(False)
            self.chart_title = "T <i>vs.</i> t Plot"
        self.chart.setTitle(self.chart_title)
        self.updated.emit()   
        
    def toggleSplines(self):
        self.splinesShown = not self.splinesShown
        self.setCurves()

    def formatAxis(self, axis, min_value, max_value, step):
        for s in axis.categoriesLabels():
            axis.remove(s)
        axis.setStartValue(min_value)
        for i in range(ceil(min_value / step), floor(max_value / step) + 1):
            v = i * step
            axis.append('%g' % v, v)
        axis.setRange(min_value, max_value)

    def keyPressEvent(self, event):
        keymap = {
            qtc.Qt.Key_Up: lambda: self.chart.scroll(0, -10),
            qtc.Qt.Key_Down: lambda: self.chart.scroll(0, 10),
            qtc.Qt.Key_Right: lambda: self.chart.scroll(-10, 0),
            qtc.Qt.Key_Left: lambda: self.chart.scroll(10, 0),
            qtc.Qt.Key_Plus: lambda: self.chart.zoomIn(),
            # alternative: zoom() method has float value zoom factor, zoomIn/Out=factor 2
            qtc.Qt.Key_Minus: lambda: self.chart.zoomOut()
            }
        callback = keymap.get(event.key())
        if callback:
            callback()


class DscVsTempView(qtw.QWidget):
    """ Display classic plot DSC vs temperature """

    tascPresent = qtc.pyqtSignal()
    tascNotPresent = qtc.pyqtSignal()
    dscOnly = False
    updated = qtc.pyqtSignal()
    dscShown = True
    tascShown = True    
    splinesShown = True
    chart_title = "DSC <i>vs.</i> T Plot"

    def __init__(self):
        super().__init__()
        # retrieve settings:
        getSettings(self)
        ### Make icons available:
        if getattr(sys, 'frozen', False):
            # compiled with PyInstaller:
            directory = sys._MEIPASS
        else:
            # run as python script:
            directory = path.dirname(__file__)
        # layout:
        self.setLayout(qtw.QHBoxLayout())
        self.view = qtch.QChartView()
        self.toolLayout = qtw.QBoxLayout(qtw.QBoxLayout.TopToBottom)
        self.toolbar = qtw.QToolBar(
            'Graph',
            orientation=qtc.Qt.Vertical)
 
        self.contentsLayout = qtw.QBoxLayout(qtw.QBoxLayout.TopToBottom)
        self.contentsLayout.addWidget(self.view)
        self.layout().addLayout(self.toolLayout)
        self.layout().addLayout(self.contentsLayout)
        self.chart = qtch.QChart(title=self.chart_title)
        self.chart.setTitleFont(self.titleFont)
        self.chart.setTitleBrush(qtg.QBrush(self.axisColor))
        self.chart.setBackgroundBrush(qtg.QBrush(self.backgroundColor))
        self.chart.layout().setContentsMargins(0, 0, 0, 0)
        self.chart.setBackgroundRoundness(0)
        self.view.setChart(self.chart)
        # toolbar:
        self.toolbar_autocol = self.toolbar.addAction('Auto-Color')
        self.toolbar_autocol.setIcon(qtg.QIcon(path.join(directory, 'icons', 'paint-palette-100.png')))

        self.seriesDsc = qtch.QSplineSeries(name="DSC")
        self.seriesDscRough = qtch.QLineSeries(name="DSC")
        self.seriesTasc = qtch.QSplineSeries(name="TASC")
        self.seriesTascRough = qtch.QLineSeries(name="TASC")
        self.chart.addSeries(self.seriesDsc)
        self.chart.addSeries(self.seriesDscRough)
        self.chart.addSeries(self.seriesTasc)
        self.chart.addSeries(self.seriesTascRough)
        self.x_axis = qtch.QCategoryAxis(
            labelsPosition=qtch.QCategoryAxis.AxisLabelsPositionOnValue,
            startValue=0.0)
        self.y_axis = qtch.QCategoryAxis(
            labelsPosition=qtch.QCategoryAxis.AxisLabelsPositionOnValue,
            startValue=0.0)
        self.y_axisTasc = qtch.QCategoryAxis(
                labelsPosition=qtch.QCategoryAxis.AxisLabelsPositionOnValue,
                startValue=0.0,
                gridVisible=False)
        self.chart.setAxisX(self.x_axis, self.seriesDsc)
        self.chart.setAxisY(self.y_axis, self.seriesDsc)
        self.chart.setAxisX(self.x_axis, self.seriesDscRough)
        self.chart.setAxisY(self.y_axis, self.seriesDscRough)
        self.chart.setAxisX(self.x_axis, self.seriesTasc)
        self.chart.setAxisX(self.x_axis, self.seriesTascRough)
        self.chart.addAxis(self.y_axisTasc, qtc.Qt.AlignRight)
        self.seriesTasc.attachAxis(self.y_axisTasc)
        self.seriesTascRough.attachAxis(self.y_axisTasc)
        self.chart.legend().setVisible(False)
        self.view.setRenderHint(qtg.QPainter.Antialiasing)

    def restyle(self):
        getSettings(self)
        self.chart.setTitleFont(self.titleFont)
        self.chart.setTitleBrush(qtg.QBrush(self.axisColor))
        self.chart.setBackgroundBrush(qtg.QBrush(self.backgroundColor))
        self.chart.update()

    def refresh(self, data):
        # retrieve updated settings:
        getSettings(self)
        # load toolbar:
        self.toolLayout.addWidget(self.toolbar)
        # (re-)apply visual settings:
        self.seriesDsc.setColor(self.dscColor)
        self.seriesDscRough.setColor(self.dscColor)
        self.seriesTasc.setColor(self.tascColor)
        self.seriesTascRough.setColor(self.tascColor)
        self.chart.legend().setFont(self.axisFont)
        self.chart.setTitleFont(self.titleFont)
        self.chart.setTitleBrush(qtg.QBrush(self.axisColor))
        self.chart.setBackgroundBrush(qtg.QBrush(self.backgroundColor))
        self.x_axis.setLabelsFont(self.axisFont)
        self.x_axis.setLabelsBrush(qtg.QBrush(self.axisColor))
        self.y_axis.setLabelsFont(self.axisFont)
        self.y_axisTasc.setLabelsFont(self.axisFont)
        self.x_axis.setTitleFont(self.axisFont)
        self.x_axis.setTitleBrush(qtg.QBrush(self.axisColor))
        self.y_axis.setLinePenColor(self.seriesDsc.pen().color())
        self.y_axis.setLabelsColor(self.seriesDsc.pen().color())
        self.y_axisTasc.setLinePenColor(self.seriesTasc.pen().color())
        self.y_axisTasc.setLabelsColor(self.seriesTasc.pen().color())

        self.data = data
        ncol = len(data[0])
        newData = []
        newDataTasc = []
        templst = []

        if ncol==6:
            for item in self.data:
                try:
                    templst.append(float(item[2]))
                except:
                    pass
            maxTemp = max(templst) + 5
            minTemp = min(templst) - 5
            templst.clear()
            for item in self.data:
                try:
                    templst.append(float(item[3]))
                except:
                    pass              
            maxDsc = max(templst) + 500
            minDsc = min(templst) - 500
            templst.clear()
            maxTasc = max([ (0.0) if x[5] in (None,'') else float(x[5]) for x in data])
            minTasc = min([ (0.0) if x[5] in (None,'') else float(x[5]) for x in data])
            for i in data:
                try:
                    newData.append(qtc.QPointF(float(i[2]), float(i[3]))) # T,DSC   
                except ValueError:
                    pass
                try:
                    newDataTasc.append(qtc.QPointF(float(i[2]), float(i[5]))) # xTASC, yTASC   
                except ValueError:
                    pass
        else:
            for item in self.data:
                try:
                    templst.append(float(item[3]))
                except:
                    pass
            maxTemp = max(templst) + 5
            minTemp = min(templst) - 5
            templst.clear()
            for item in self.data:
                try:
                    templst.append(float(item[4]))
                except:
                    pass              
            maxDsc = max(templst) + 500
            minDsc = min(templst) - 500
            templst.clear()
            maxTasc = max([ (0.0) if x[6] in (None,'') else float(x[6]) for x in data])
            minTasc = min([ (0.0) if x[6] in (None,'') else float(x[6]) for x in data])
            for i in data:
                try:
                    newData.append(qtc.QPointF(float(i[3]), float(i[4]))) # t,T   
                except ValueError:
                    pass
                try:
                    newDataTasc.append(qtc.QPointF(float(i[2]), float(i[6]))) # T, yTASC   
                except ValueError:
                    pass
        # TASC detection/toggling: 
        if len(newDataTasc) > 0:
            self.tascPresent.emit()
            self.dscOnly = False
            if self.tascShown == True:
                self.seriesTasc.setVisible(True)
                self.y_axisTasc.setVisible(True)
        else:
            self.tascNotPresent.emit()
            self.seriesTasc.setVisible(False)
            self.y_axisTasc.setVisible(False)
            self.dscOnly = True
        self.chart.legend().setVisible(True)
        # format & attach axis to both chart & series, toggle K/°C:
        if self.kelvin:
            maxTemp += 273.15
            minTemp += 273.15
            newData[:] = [ qtc.QPointF(i.x()+273.15, i.y()) for i in newData ]
            newDataTasc[:] = [ qtc.QPointF(i.x()+273.15, i.y()) for i in newDataTasc ]
            self.x_axis.setTitleText('T / K')
        else:
            self.x_axis.setTitleText('T / °C')
            
        try:
            self.formatAxis(self.x_axis,
                        minTemp-max(abs(minTemp),abs(maxTemp))*0.05,
                        maxTemp+max(abs(minTemp),abs(maxTemp))*0.05,
                        round(abs(maxTemp-minTemp)/8,-1))
        except:
            pass
        try:
            self.formatAxis(self.y_axis,
                        minDsc-max(abs(minDsc),abs(maxDsc))*0.05,
                        maxDsc+max(abs(minDsc),abs(maxDsc))*0.05,
                        round(abs(maxDsc-minDsc)/6,-3))
        except:
            pass
        try:
            self.formatAxis(self.y_axisTasc,
                        minTasc-max(abs(minTasc),abs(maxTasc))*0.05,
                        maxTasc+max(abs(minTasc),abs(maxTasc))*0.05,
                        round(abs(maxTasc-minTasc)/6,1))
        except:
            pass
        # update data in series:
        self.seriesDsc.replace(newData)
        self.seriesDscRough.replace(newData)
        self.seriesTasc.replace(newDataTasc)
        self.seriesTascRough.replace(newDataTasc)
        # make rough curves invisible initially:
        self.seriesDscRough.setVisible(False)
        self.seriesTascRough.setVisible(False)
        # call switched functions:
        self.setCurves()

    def toggleTasc(self):
        self.tascShown = not self.tascShown
        self.setCurves()

    def setCurves(self):
        # DSC shown
        if (self.dscShown and not self.tascShown) or (self.dscShown and self.dscOnly):
            self.x_axis.setVisible(True)
            self.y_axis.setVisible(True)
            self.y_axisTasc.setVisible(False)
            self.y_axisTasc.setGridLineVisible(False)
            self.chart_title = "DSC <i>vs.</i> T Plot"
            self.seriesTasc.setVisible(False)
            self.seriesTascRough.setVisible(False)
            if self.splinesShown == True:
                self.seriesDsc.setVisible(True)
                self.seriesDscRough.setVisible(False)
            else:
                self.seriesDsc.setVisible(False)
                self.seriesDscRough.setVisible(True)
        # TASC shown:
        elif self.tascShown and not self.dscShown and not self.dscOnly:
            self.x_axis.setVisible(True)
            self.y_axis.setVisible(False)
            self.y_axisTasc.setVisible(True)
            self.y_axisTasc.setGridLineVisible(True)
            self.chart_title = "TASC <i>vs.</i> T Plot"
            self.seriesDsc.setVisible(False)
            self.seriesDscRough.setVisible(False)
            if self.splinesShown == True:
                self.seriesTasc.setVisible(True)
                self.seriesTascRough.setVisible(False)
            else:
                self.seriesTasc.setVisible(False)
                self.seriesTascRough.setVisible(True)
        # DSC & TASC shown:
        elif self.dscShown and self.tascShown:
            self.x_axis.setVisible(True)
            self.y_axis.setVisible(True)
            self.y_axisTasc.setVisible(True)
            self.y_axisTasc.setGridLineVisible(False)
            self.chart_title = "DSC/TASC <i>vs.</i> T Plot"
            if self.splinesShown == True:
                self.seriesDsc.setVisible(True)
                self.seriesDscRough.setVisible(False)
                self.seriesTasc.setVisible(True)
                self.seriesTascRough.setVisible(False)
            else:
                self.seriesDsc.setVisible(False)
                self.seriesDscRough.setVisible(True)
                self.seriesTasc.setVisible(False)
                self.seriesTascRough.setVisible(True)
        #nothing shown:
        else:
            self.seriesDsc.setVisible(False)
            self.seriesDscRough.setVisible(False)
            self.seriesTasc.setVisible(False)
            self.seriesTascRough.setVisible(False)
            self.x_axis.setVisible(False)
            self.y_axis.setVisible(False)
            self.y_axisTasc.setVisible(False)
            self.y_axisTasc.setGridLineVisible(False)
            self.chart_title = "DSC <i>vs.</i> T Plot"
        self.chart.setTitle(self.chart_title)
        self.updated.emit()            

    def toggleDsc(self):
        self.dscShown = not self.dscShown
        self.setCurves()
       
    def toggleSplines(self):
        self.splinesShown = not self.splinesShown
        self.setCurves()

    def formatAxis(self, axis, min_value, max_value, step):
        for s in axis.categoriesLabels():
            axis.remove(s)
        axis.setStartValue(min_value)
        for i in range(ceil(min_value / step), floor(max_value / step) + 1):
            v = i * step
            axis.append('%g' % v, v)
        axis.setRange(min_value, max_value)

    def keyPressEvent(self, event):
        keymap = {
            qtc.Qt.Key_Up: lambda: self.chart.scroll(0, -10),
            qtc.Qt.Key_Down: lambda: self.chart.scroll(0, 10),
            qtc.Qt.Key_Right: lambda: self.chart.scroll(-10, 0),
            qtc.Qt.Key_Left: lambda: self.chart.scroll(10, 0),
            qtc.Qt.Key_Plus: lambda: self.chart.zoomIn(),
            # alternative: zoom() method has float value zoom factor, zoomIn/Out=factor 2
            qtc.Qt.Key_Minus: lambda: self.chart.zoomOut()
            }
        callback = keymap.get(event.key())
        if callback:
            callback()
