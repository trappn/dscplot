import sys
import re
from os import path
from datetime import datetime

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
from PyQt5 import QtPrintSupport as qtps

from .auxiliary import getSettings


class PrintPreview(qtw.QTextEdit):

    dpi = 150

    def __init__(self, printObj):
        super().__init__(readOnly=True)
        self.printer = printObj
        # retrieve settings:
        getSettings(self)

    def generate(self, items, leftView, rightView, data):
        pwidth = 0.8*self.printer.pageLayout().paintRectPixels(self.dpi).width()
        pheight = 0.3*self.printer.pageLayout().paintRectPixels(self.dpi).height()
        input_size = qtc.QSize(leftView.width(),leftView.height())
        input_rect = qtc.QRect(qtc.QPoint(0,0),qtc.QSize(input_size))
        output_size = qtc.QSize(pwidth,pheight)
        output_rect = qtc.QRectF(qtc.QPointF(0,0),qtc.QSizeF(output_size))
        document = qtg.QTextDocument()
        paperSize = qtc.QSizeF()
        paperSize.setWidth(self.printer.width())
        paperSize.setHeight(self.printer.height())
        document.setPageSize(paperSize)
        self.setDocument(document)
        cursor = qtg.QTextCursor(document)
        root = document.rootFrame()
        cursor.setPosition(root.firstPosition())
        # define frame formats:
        frame_fmt = qtg.QTextFrameFormat()
        frame_fmt.setBorder(0)
        frame_fmt.setPadding(0)
        # define text formats:
        label_format = qtg.QTextCharFormat()
        label_format.setFont(qtg.QFont('Sans', 12, qtg.QFont.Bold))
        std_format = qtg.QTextCharFormat()
        std_format.setFont(qtg.QFont('Sans', 8, qtg.QFont.Normal))
        # append checked items:
        if 'Data File Name' in items.get('checkedCheckbuttons'):
            header_frame = cursor.insertFrame(frame_fmt)
            cursor.setPosition(header_frame.firstPosition())
            cursor.insertText(data.filename, label_format)
        if 'Date' in items.get('checkedCheckbuttons'):
            cursor.setPosition(root.lastPosition())
            date_frame = cursor.insertFrame(frame_fmt)
            cursor.setPosition(date_frame.firstPosition())
            dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
            cursor.insertText(dt_string, std_format)
        if 'Both Plots' in items.get('checkedRadiobuttons'):
            cursor.setPosition(root.lastPosition())
            left_frame = cursor.insertFrame(frame_fmt)
            cursor.setPosition(left_frame.firstPosition())
            leftImg = qtg.QImage(output_size, qtg.QImage.Format_RGB888)
            leftImg.fill(qtc.Qt.white)
            painter = qtg.QPainter(leftImg)
            painter.setRenderHint(qtg.QPainter.Antialiasing)
            painter.setViewport(leftImg.rect())
            painter.setWindow(leftImg.rect())
            leftView.view.render(painter,
                                 source=input_rect,
                                 target=output_rect,
                                 mode=qtc.Qt.KeepAspectRatio)            
            cursor.insertImage(leftImg)
            painter.end()
            cursor.setPosition(root.lastPosition())
            right_frame = cursor.insertFrame(frame_fmt)
            cursor.setPosition(right_frame.firstPosition())
            rightImg = qtg.QImage(output_size, qtg.QImage.Format_RGB888)
            rightImg.fill(qtc.Qt.white)
            painter = qtg.QPainter(rightImg)
            painter.setRenderHint(qtg.QPainter.Antialiasing)
            painter.setViewport(rightImg.rect())
            painter.setWindow(rightImg.rect())
            rightView.view.render(painter,
                                 source=input_rect,
                                 target=output_rect,
                                 mode=qtc.Qt.KeepAspectRatio)            
            cursor.insertImage(rightImg)
            painter.end()
        if 'DSC/Time Plot' in items.get('checkedRadiobuttons'):
            cursor.setPosition(root.lastPosition())
            left_frame = cursor.insertFrame(frame_fmt)
            cursor.setPosition(left_frame.firstPosition())
            leftImg = qtg.QImage(output_size, qtg.QImage.Format_RGB888)
            leftImg.fill(qtc.Qt.white)
            painter = qtg.QPainter(leftImg)
            painter.setRenderHint(qtg.QPainter.Antialiasing)
            painter.setViewport(leftImg.rect())
            painter.setWindow(leftImg.rect())
            leftView.view.render(painter,
                                 source=input_rect,
                                 target=output_rect,
                                 mode=qtc.Qt.KeepAspectRatio)            
            cursor.insertImage(leftImg)
            painter.end()
        if 'DSC/Temp Plot' in items.get('checkedRadiobuttons'):
            cursor.setPosition(root.lastPosition())
            right_frame = cursor.insertFrame(frame_fmt)
            cursor.setPosition(right_frame.firstPosition())
            rightImg = qtg.QImage(output_size, qtg.QImage.Format_RGB888)
            rightImg.fill(qtc.Qt.white)
            painter = qtg.QPainter(rightImg)
            painter.setRenderHint(qtg.QPainter.Antialiasing)
            painter.setViewport(rightImg.rect())
            painter.setWindow(rightImg.rect())
            rightView.view.render(painter,
                                 source=input_rect,
                                 target=output_rect,
                                 mode=qtc.Qt.KeepAspectRatio)            
            cursor.insertImage(rightImg)
            painter.end()
        if 'Notes' in items.get('checkedCheckbuttons'):
            try:
                if len(data.notes) > 0:
                    cursor.setPosition(root.lastPosition())
                    notes_frame = cursor.insertFrame(frame_fmt)
                    cursor.setPosition(notes_frame.firstPosition())
                    cursor.insertText('NOTES:\n\n', std_format)
                    cursor.insertText(data.notes, std_format)
            except:
                pass


class PrintSettingsTab(qtw.QWidget):

    changed = qtc.pyqtSignal()

    def __init__(self, changed=None):                    
        super().__init__()
        if changed:
            self.changed.connect(changed)
        self.setFixedSize(qtc.QSize(450,180))
        buttonSize = qtc.QSize(110,35)
        self.cbFilename = qtw.QCheckBox('Data File Name')
        self.cbDate = qtw.QCheckBox('Date')
        self.cbMeta = qtw.QCheckBox('Notes')
        self.rbBoth = qtw.QRadioButton('Both Plots')
        self.rbLeft = qtw.QRadioButton('DSC/Time Plot')
        self.rbRight = qtw.QRadioButton('DSC/Temp Plot')
        self.optbox = qtw.QGroupBox(
            'Options',
            alignment=qtc.Qt.AlignLeft,
            )
        self.optbox.setFixedSize(qtc.QSize(435,130))
        self.optbox.setLayout(qtw.QGridLayout())
        self.optbox.layout().addWidget(self.cbFilename,0,1)
        self.optbox.layout().addWidget(self.cbDate,1,1)
        self.optbox.layout().addWidget(self.cbMeta,2,1)
        self.optbox.layout().addWidget(self.rbBoth,0,0)
        self.optbox.layout().addWidget(self.rbLeft,1,0)
        self.optbox.layout().addWidget(self.rbRight,2,0)
        self.setLayout(qtw.QVBoxLayout())
        self.layout().addWidget(self.optbox)
        # signals:
        for item in self.findChildren(qtw.QRadioButton):
            item.toggled.connect(self.on_change)
        for item in self.findChildren(qtw.QCheckBox):
            item.toggled.connect(self.on_change)
                        
    def on_change(self):
        self.changed.emit()


class PrintSettingsWindow(qtw.QWidget):             

    submitted = qtc.pyqtSignal()
    defaults = {
        'checkedRadiobuttons' : ['Both Plots'],
        'checkedCheckbuttons' : ['Data File Name','Date']
    }

    def __init__(self, parent, leftView, rightView, printObj, data, pdf=False):                    
        super().__init__()
        self.parent = parent
        self.data = data
        self.pdf = pdf
        # handle exit condition:
        parent.destroyed.connect(self.close)
        # retrieve settings:
        getSettings(self)
        self.leftView = leftView
        self.rightView = rightView
        self.items = self.settings.value('printsettings', self.defaults)
        self.setFixedSize(qtc.QSize(500,260))
        buttonSize = qtc.QSize(110,35)
        layout = qtw.QVBoxLayout()
        self.setLayout(layout)
        # references to subsettings:
        self.pst = PrintSettingsTab()
        self.buttonbox = qtw.QWidget()
        self.buttonbox.setFixedHeight(60)
        if pdf == True:
            self.setWindowTitle('PDF Settings')
            self.applyButton = qtw.QPushButton('OK', clicked=self.on_applyPDF)
        else:
            self.setWindowTitle('Print Settings')
            self.applyButton = qtw.QPushButton('OK', clicked=self.on_apply)
        self.cancelButton = qtw.QPushButton('Cancel', clicked=self.on_close)
        self.applyButton.setDefault(True)
        self.cancelButton.setFixedSize(buttonSize)
        self.applyButton.setFixedSize(buttonSize)
        buttonlayout = qtw.QHBoxLayout()
        self.buttonbox.setLayout(buttonlayout)
        buttonlayout.setAlignment(qtc.Qt.AlignRight)
        buttonlayout.addWidget(self.cancelButton)
        buttonlayout.addWidget(self.applyButton)
        # add all the widgets to layout:
        layout.addWidget(self.pst)
        layout.addWidget(self.buttonbox)
        # Configure printer, preview & defaults:
        self.printer = printObj
        self.printer.setOrientation(qtps.QPrinter.Portrait)
        #self.printer.setPageSize(qtg.QPageSize(qtg.QPageSize.A4))
        self.preview = PrintPreview(self.printer)
        # load presets
        self.recover_settings()
        # End main UI code
        self.show() 
 
    def keyPressEvent(self, qKeyEvent):
        # Note: This is a workaround, because default buttons apparently only work in children of QDialog et al.
        if qKeyEvent.key() == qtc.Qt.Key_Return: 
            self.on_apply()
        else:
            super().keyPressEvent(qKeyEvent)            

    def recover_settings(self):
        rbuttons = self.pst.findChildren(qtw.QRadioButton)
        rcheck = self.pst.findChildren(qtw.QCheckBox)
        for item in rbuttons:
            if item.text() in self.items.get('checkedRadiobuttons'):
                item.setChecked(True)
        for item in rcheck:
            if item.text() in self.items.get('checkedCheckbuttons'):
                item.setChecked(True)
        
    def on_apply(self):
        self.submitted.emit()
        self.get_currentItems()
        self.settings.setValue('printsettings', self.items)
        self.preview.generate(self.items, self.leftView, self.rightView, self.data)
        self.print_preview()
        self.close()

    def on_applyPDF(self):
        self.submitted.emit()
        self.get_currentItems()
        self.settings.setValue('printsettings', self.items)
        self.preview.generate(self.items, self.leftView, self.rightView, self.data)
        try:
            directory = self.settings.value('lastdir')
        except:
            directory = qtc.QDir.homePath()
        filename, ext = qtw.QFileDialog.getSaveFileName(
            self,
            'Save to PDF...',
            directory,
            'PDF File (*.pdf)',
            options=qtw.QFileDialog.DontUseNativeDialog
            )
        if filename:
            self.settings.setValue('lastdir', qtc.QFileInfo(filename).absolutePath())
            # remove extra extensions:
            filename = path.splitext(filename)[0]
            filename = filename + '.pdf'
            self.printer.setOutputFileName(filename)
            self.printer.setOutputFormat(qtps.QPrinter.PdfFormat)
            self.print_document()
            self.parent.statusBar().showMessage(f'{filename} : saved (PDF)')
        else:
            self.parent.statusBar().showMessage('PDF export cancelled')
        self.close()

    def print_preview(self):
        dialog = qtps.QPrintPreviewDialog(self.printer, self)
        dialog.paintRequested.connect(self.print_document)
        dialog.exec()

    def print_document(self):
        # this just paints the document to the printer object
        self.preview.document().print(self.printer)

    def get_currentItems(self):
        # collect data from widgets:
        rbuttons = self.pst.findChildren(qtw.QRadioButton)
        rcheck = self.pst.findChildren(qtw.QCheckBox)
        rbuttonlist = []
        checkblist = []
        for item in rbuttons:
            if item.isChecked():
                rbuttonlist.append(item.text())
        for item in rcheck:
            if item.isChecked():
                checkblist.append(item.text())
        self.items = {
            'checkedRadiobuttons' : rbuttonlist,
            'checkedCheckbuttons' : checkblist         
        }    
        
    def on_close(self):
        if self.pdf == True:
            self.parent.statusBar().showMessage('PDF export cancelled')
        else:
            self.parent.statusBar().showMessage('print cancelled')
        self.close()


class dragList(qtw.QListWidget):
    """ Handles both file drops and internal drag&drop for sorting """

    dropped = qtc.pyqtSignal(list)
    
    def __init__(self,parent):
        super(dragList,self).__init__(parent);
        self.setAcceptDrops(True);
        self.setDragDropMode(qtw.QAbstractItemView.InternalMove)
        self.setDefaultDropAction(qtc.Qt.MoveAction)
        #self.setDefaultDropAction(qtc.Qt.CopyAction)
        # Important for Drag&Drop action without delete:
        #self.setDragDropMode(qtw.QAbstractItemView.InternalMove)
    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction() ;
        else:
            super(dragList,self).dragEnterEvent(event);

    def dragMoveEvent(self, event):
        super(dragList,self).dragMoveEvent(event);

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(qtc.Qt.CopyAction)
            event.accept()
            links=[]
            for url in event.mimeData().urls():
                links.append(str(url.toLocalFile()))
            self.dropped.emit(links)
        else:
            super(dragList,self).dropEvent(event)
            

class MultiPdfSettingsTab(qtw.QWidget):

    changed = qtc.pyqtSignal()

    def __init__(self, parent, changed=None):                    
        super().__init__()
        if changed:
            self.changed.connect(changed)
        self.setFixedSize(qtc.QSize(760,520))
        buttonSize = qtc.QSize(110,35)
        self.rb1 = qtw.QRadioButton('1 per page')
        self.rb3 = qtw.QRadioButton('3 per page')
        self.rb5 = qtw.QRadioButton('5 per page')
        self.buttonGroupRight = qtw.QButtonGroup()
        self.buttonGroupRight.addButton(self.rb1, 1)
        self.buttonGroupRight.addButton(self.rb3, 2)
        self.buttonGroupRight.addButton(self.rb5, 3)        
        self.rbBoth = qtw.QRadioButton('Both Plots')
        self.rbLeft = qtw.QRadioButton('DSC/Time Plot')
        self.rbRight = qtw.QRadioButton('DSC/Temp Plot')
        self.buttonGroupLeft = qtw.QButtonGroup()
        self.buttonGroupLeft.addButton(self.rbBoth, 1)
        self.buttonGroupLeft.addButton(self.rbLeft, 2)
        self.buttonGroupLeft.addButton(self.rbRight, 3)
        self.cbMeta = qtw.QCheckBox('Include Notes')        
        self.optbox = qtw.QGroupBox(
            'Options',
            alignment=qtc.Qt.AlignLeft,
            )
        self.optbox.setFixedSize(qtc.QSize(620,140))
        self.optbox.setLayout(qtw.QGridLayout())
        self.optbox.layout().addWidget(self.rb1,0,1)
        self.optbox.layout().addWidget(self.rb3,1,1)
        self.optbox.layout().addWidget(self.rb5,2,1)
        self.optbox.layout().addWidget(self.rbBoth,0,0)
        self.optbox.layout().addWidget(self.rbLeft,1,0)
        self.optbox.layout().addWidget(self.rbRight,2,0)
        self.optbox.layout().addWidget(self.cbMeta,0,2)
        # file list/control buttons&layouts:
        self.listlayout = qtw.QHBoxLayout()
        self.controllayout = qtw.QVBoxLayout()
        self.controllayout.setAlignment(qtc.Qt.AlignTop)
        self.filelist = dragList(parent)
        self.filelist.setToolTip('Drag & Drop to change file ordering')
        self.filelist.setFixedSize(qtc.QSize(620,310))
        self.filelist.setSelectionMode(qtw.QAbstractItemView.ExtendedSelection)
        self.filelist.setAlternatingRowColors(True)
        self.setLayout(qtw.QVBoxLayout())
        self.addButton = qtw.QPushButton('Add Files', clicked=self.on_change)
        self.removeButton = qtw.QPushButton('Remove Files', clicked=self.on_change)
        self.addButton.setFixedSize(buttonSize)
        self.removeButton.setFixedSize(buttonSize)
        self.removeButton.setDisabled(True)
        self.controllayout.addWidget(self.addButton)
        self.controllayout.addWidget(self.removeButton)
        self.listlayout.addWidget(self.filelist)
        self.listlayout.addLayout(self.controllayout)
        self.layout().addLayout(self.listlayout)
        self.layout().addWidget(self.optbox)
        # signals:
        self.filelist.dropped.connect(self.handledrop)
        for item in self.findChildren(qtw.QRadioButton):
            item.toggled.connect(self.on_change)
        for item in self.findChildren(qtw.QCheckBox):
            item.toggled.connect(self.on_change)

    def handledrop(self, links):
        for url in links:
            if path.exists(url):
                # remove invalid extensions:
                ext = path.splitext(url)[1]
                if ext in ['.dsc', '.txt', '.csv']:
                    item = qtw.QListWidgetItem(url, self.filelist)
                        
    def on_change(self):
        self.changed.emit()
        

class MultiPdfSettingsWindow(qtw.QWidget):             

    submitted = qtc.pyqtSignal()
    defaults = {
        'checkedRadiobuttons' : ['Both Plots', '3 per page'],
        'checkedCheckbuttons' : ['Include Notes']
    }

    def __init__(self, parent, printObj):                    
        super().__init__()
        self.parent = parent
        # handle exit condition:
        parent.destroyed.connect(self.close)
        # retrieve settings:
        getSettings(self)
        self.items = self.settings.value('multipdfsettings', self.defaults)
        self.setFixedSize(qtc.QSize(800,600))
        buttonSize = qtc.QSize(110,35)
        layout = qtw.QVBoxLayout()
        self.setLayout(layout)
        # references to subsettings:
        self.mst = MultiPdfSettingsTab(parent)
        self.buttonbox = qtw.QWidget()
        self.buttonbox.setFixedHeight(60)
        self.setWindowTitle('multi-PDF Settings')
        self.applyButton = qtw.QPushButton('OK', clicked=self.on_apply)
        self.cancelButton = qtw.QPushButton('Cancel', clicked=self.on_close)
        self.applyButton.setDefault(True)
        self.cancelButton.setFixedSize(buttonSize)
        self.applyButton.setFixedSize(buttonSize)
        self.applyButton.setDisabled(True)
        buttonlayout = qtw.QHBoxLayout()
        self.buttonbox.setLayout(buttonlayout)
        buttonlayout.setAlignment(qtc.Qt.AlignRight)
        buttonlayout.addWidget(self.cancelButton)
        buttonlayout.addWidget(self.applyButton)
        # add all the widgets to layout:
        layout.addWidget(self.mst)
        layout.addWidget(self.buttonbox)
        # signals:
        self.mst.addButton.clicked.connect(self.add_files)
        self.mst.removeButton.clicked.connect(self.remove_files)
        self.mst.filelist.itemClicked.connect(self.toggle_remove)
        # Configure printer, preview & defaults:
        self.printer = printObj
        self.printer.setOrientation(qtps.QPrinter.Portrait)
        #self.printer.setPageSize(qtg.QPageSize(qtg.QPageSize.A4))
        #self.preview = PrintPreview(self.printer)  <--- add class for multi-page/pdf preview here
        # load presets
        self.recover_settings()
        # End main UI code
        self.show() 

    def toggle_remove(self, selection):
        selected = False
        for num in range(0, self.mst.filelist.count()):
            if self.mst.filelist.item(num).isSelected():
                selected = True
        if selected:
            self.mst.removeButton.setEnabled(True)
        else:
            self.mst.removeButton.setDisabled(True)

    def add_files(self, files=None):
        if not files:
            files = self.get_files_from_dialog()
        if files:
            self.applyButton.setEnabled(True)
        else:
            return
        for n, file in enumerate(files):
            if file:
                tree_item = qtw.QListWidgetItem()
                self.mst.filelist.addItem(tree_item)
                tree_item.setText(file)

    def remove_files(self):
        items = self.mst.filelist.selectedItems()
        if not items:
            return        
        for i in items:
            self.mst.filelist.takeItem(self.mst.filelist.row(i))
        if self.mst.filelist.count() == 0:
            self.applyButton.setDisabled(True)
            self.mst.removeButton.setDisabled(True)

    def get_files_from_dialog(self):
        # default browse directory (last used or user home):
        try:
            directory = self.settings.value('lastdir')
        except:
            directory = qtc.QDir.homePath()
        files, _ = qtw.QFileDialog.getOpenFileNames(
            self,
            'Open File(s)',
            directory,
            'All Files (*);;DSCPlot Project (*.dsc);;ASCII File - LINK/DSCPlot (*.txt);;CSV Spreadsheet - dscplot (*.csv)',
            options=qtw.QFileDialog.DontUseNativeDialog
            )
        if files:
            self.settings.setValue('lastdir', qtc.QFileInfo(files[-1]).absolutePath())
        return files

    def keyPressEvent(self, qKeyEvent):
        # Note: This is a workaround, because default buttons apparently only work in children of QDialog et al.
        if qKeyEvent.key() == qtc.Qt.Key_Return: 
            self.on_apply()
        else:
            super().keyPressEvent(qKeyEvent)    

    def recover_settings(self):
        rbuttons = self.mst.findChildren(qtw.QRadioButton)
        rcheck = self.mst.findChildren(qtw.QCheckBox)
        for item in rbuttons:
            if item.text() in self.items.get('checkedRadiobuttons'):
                item.setChecked(True)
        for item in rcheck:
            if item.text() in self.items.get('checkedCheckbuttons'):
                item.setChecked(True)

    def on_apply(self):
        self.submitted.emit()
        self.get_currentItems()
        self.settings.setValue('multipdfsettings', self.items)
        # this part straight from DK
        files_list = []
        for num in range(self.mst.filelist.count()):
            item = self.mst.filelist.item(num)
            itemtxt = item.text()
            files_list.append(itemtxt)
        if not files_list:
            return
        # default browse directory (last used or user home):
        try:
            directory = self.settings.value('lastdir')
        except:
            directory = qtc.QDir.homePath()
        filename, ext = qtw.QFileDialog.getSaveFileName(
            self,
            'Save to PDF (multiple data sets)...',
            directory,
            'PDF File (*.pdf)',
            options=qtw.QFileDialog.DontUseNativeDialog
            )
        if filename:
            self.settings.setValue('lastdir', qtc.QFileInfo(filename).absolutePath())
            # remove extra extensions:
            filename = path.splitext(filename)[0]
            filename = filename + '.pdf'
            self.printer.setOutputFileName(filename)
            self.printer.setOutputFormat(qtps.QPrinter.PdfFormat)
            self.print_document()
            self.parent.statusBar().showMessage(f'{filename} : saved (multi-PDF)')
        else:
            self.parent.statusBar().showMessage('multi-PDF export cancelled')
        print('run me here')
        # code to actually assemble the file here: multitable.make_report_from(files_list, output_filename)
        self.mst.filelist.clear()
        # /this part straight from DK
        self.close()

    def get_currentItems(self):
        # collect data from widgets:
        rbuttons = self.mst.findChildren(qtw.QRadioButton)
        rcheck = self.mst.findChildren(qtw.QCheckBox)
        rbuttonlist = []
        checkblist = []
        for item in rbuttons:
            if item.isChecked():
                rbuttonlist.append(item.text())
        for item in rcheck:
            if item.isChecked():
                checkblist.append(item.text())
        self.items = {
            'checkedRadiobuttons' : rbuttonlist,
            'checkedCheckbuttons' : checkblist         
        }    
    
    def on_close(self):
        self.parent.statusBar().showMessage('multi-PDF export cancelled')
        self.close()
