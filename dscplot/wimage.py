import sys
import re
from os import path

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

from .auxiliary import getSettings


class IntegerNumberValidator(qtg.QIntValidator):
    """Validates an integer number in a given range
    """

    def validate(self, string, index):
        try:
            val = int(string)
            if val > 10000:
                state = qtg.QValidator.Invalid
            elif val < 1:
                state = qtg.QValidator.Invalid
            else:
                state = qtg.QValidator.Acceptable
        except:
            state = qtg.QValidator.Invalid
        return (state, string, index)


class CustomResolutionInput(qtw.QWidget):
    
    changed = qtc.pyqtSignal()

    def __init__(self, currentW, currentH, changed=None):                    
        super().__init__()
        if changed:
            self.changed.connect(changed)
        self.currentW = currentW
        self.currentH = currentH
        layout = qtw.QHBoxLayout()
        layout.setContentsMargins(0,0,0,0)
        self.setLayout(layout)
        self.leCustomW = qtw.QLineEdit(str(self.currentW))
        self.leCustomH = qtw.QLineEdit(str(self.currentH))
        self.leCustomW.setFixedWidth(60)
        self.leCustomH.setFixedWidth(60)  
        self.leCustomLabelCtr = qtw.QLabel(' x ')
        self.leCustomLabelRght = qtw.QLabel(' px')
        layout.addWidget(self.leCustomW)
        layout.addWidget(self.leCustomLabelCtr)
        layout.addWidget(self.leCustomH)
        layout.addWidget(self.leCustomLabelRght)
        self.leCustomW.setDisabled(True)
        self.leCustomH.setDisabled(True)
        # validator (integer, 1-10000)
        self.leCustomW.setValidator(IntegerNumberValidator())
        self.leCustomH.setValidator(IntegerNumberValidator())
        # signals:
        self.leCustomW.textChanged.connect(self.on_change)
        self.leCustomH.textChanged.connect(self.on_change)
        
    def on_enable(self):
        self.leCustomW.setDisabled(False)
        self.leCustomH.setDisabled(False)
    
    def on_disable(self):
        self.leCustomW.setDisabled(True)
        self.leCustomH.setDisabled(True)
 
    def on_change(self):
        self.changed.emit()


class ImagePreviewTab(qtw.QWidget):

    changed = qtc.pyqtSignal()

    def __init__(self, changed=None):                    
        super().__init__()
        if changed:
            self.changed.connect(changed)
        self.setFixedSize(qtc.QSize(450,450))
        buttonSize = qtc.QSize(110,35)
        layout = qtw.QVBoxLayout()
        self.label = qtw.QLabel()
        self.label.setFixedSize(450, 450)
        self.label.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)      
        layout.addWidget(self.label)
        self.setLayout(layout)
        

    def setImage(self, image):
        self.label.setPixmap(image.scaled(450,450,qtc.Qt.KeepAspectRatio))
        self.label.repaint()
        
    def setText(self, string):
        self.label.setText(string)

    def on_change(self):
        self.changed.emit()
        

class ImageSettingsTab(qtw.QWidget):

    changed = qtc.pyqtSignal()

    def __init__(self, currentW, currentH, fileformat,changed=None):                    
        super().__init__()
        if changed:
            self.changed.connect(changed)
        self.currentW = currentW
        self.currentH = currentH
        self.setFixedSize(qtc.QSize(450,450))
        buttonSize = qtc.QSize(110,35)
        self.lbCurrent = qtw.QLabel(str(self.currentW)+' x '+str(self.currentH)+' px')
        self.rbCurrent = qtw.QRadioButton('Current Chart Size:')
        self.rbMultiple = qtw.QRadioButton('Multiple of Current Size:')
        self.sbMultiple = qtw.QDoubleSpinBox(
            self,
            value=1,
            maximum=20,
            minimum=0.1,
            singleStep=1
            )
        self.sbMultiple.setFixedWidth(80)
        self.sbMultiple.setDisabled(True)
        self.rb640 = qtw.QRadioButton('640 x 480 px')
        self.rb800 = qtw.QRadioButton('800 x 600 px')
        self.rb1024 = qtw.QRadioButton('1024 x 768 px')
        self.rbCustom = qtw.QRadioButton('Custom Size:')
        self.cbAnnotate = qtw.QCheckBox('Annotations')
        self.cbTransparent = qtw.QCheckBox('Transparent Background')
        self.cbGreyscale = qtw.QCheckBox('Grayscale')
        self.rbBoth = qtw.QRadioButton('Both Plots')
        self.rbLeft = qtw.QRadioButton('DSC/Time Plot')
        self.rbRight = qtw.QRadioButton('DSC/Temp Plot')
        self.resbox = qtw.QGroupBox(
            'Resolution',
            alignment=qtc.Qt.AlignLeft,
            )
        self.optbox = qtw.QGroupBox(
            'Options',
            alignment=qtc.Qt.AlignLeft,
            )
        self.customRes = CustomResolutionInput(self.currentW, self.currentH)
        self.resbox.setFixedSize(qtc.QSize(435,240))
        self.optbox.setFixedSize(qtc.QSize(435,130))
        self.resbox.setLayout(qtw.QGridLayout())
        self.optbox.setLayout(qtw.QGridLayout())
        self.resbox.layout().addWidget(self.rbCurrent,0,0)
        self.resbox.layout().addWidget(self.lbCurrent,0,1)
        self.resbox.layout().addWidget(self.rbMultiple,1,0)
        self.resbox.layout().addWidget(self.sbMultiple,1,1)
        self.resbox.layout().addWidget(self.rb640,2,0)
        self.resbox.layout().addWidget(self.rb800,3,0)
        self.resbox.layout().addWidget(self.rb1024,4,0)
        self.resbox.layout().addWidget(self.rbCustom,5,0)
        self.resbox.layout().addWidget(self.customRes,5,1)
        self.optbox.layout().addWidget(self.cbAnnotate,0,1)
        self.optbox.layout().addWidget(self.cbTransparent,2,1)
        self.optbox.layout().addWidget(self.cbGreyscale,1,1)
        self.optbox.layout().addWidget(self.rbBoth,0,0)
        self.optbox.layout().addWidget(self.rbLeft,1,0)
        self.optbox.layout().addWidget(self.rbRight,2,0)
        self.setLayout(qtw.QVBoxLayout())
        self.layout().addWidget(self.resbox)
        self.layout().addWidget(self.optbox)
        # signals:
        self.rbMultiple.toggled.connect(self.toggleEdit)
        self.rbCustom.toggled.connect(self.toggleEdit)
        # disable transparency for not applicable formats:
        if fileformat not in ['PNG']:
            self.cbTransparent.setDisabled(True)
        # signals:
        for item in self.findChildren(qtw.QRadioButton):
            item.toggled.connect(self.on_change)
        for item in self.findChildren(qtw.QCheckBox):
            item.toggled.connect(self.on_change)
        self.sbMultiple.valueChanged.connect(self.on_change)
        self.customRes.changed.connect(self.on_change)
        
    def toggleEdit(self):
        if self.rbCustom.isChecked():
            self.customRes.on_enable()
        else:
            self.customRes.on_disable()
        if self.rbMultiple.isChecked():
            self.sbMultiple.setDisabled(False)
        else:
            self.sbMultiple.setDisabled(True)

    def on_change(self):
        self.changed.emit()


class ExportImageSettingsWindow(qtw.QWidget):             

    submitted = qtc.pyqtSignal()
    currentW = '1920'
    currentH = '1080'
    defaults = {
        'checkedRadiobuttons' : ['Current Chart Size:', 'Both Plots'],
        'checkedCheckbuttons' : ['Annotations'],
        'customWidth': 1920,
        'customHeight' : 1080,
        'customMultiplier' : 1.00
    }

    def __init__(self, parent, leftView, rightView, applied=None):                    
        super().__init__()
        self.parent = parent
        self.image = qtg.QPixmap()
        self.output_rect = qtc.QRectF(qtc.QPointF(0,0),qtc.QSizeF(1,1))
        self.double_output_rect = qtc.QRectF(qtc.QPointF(0,0),qtc.QSizeF(1,1))
        # handle exit condition:
        parent.destroyed.connect(self.close)
        # retrieve settings:
        getSettings(self)
        self.leftView = leftView
        self.rightView = rightView
        self.data = self.settings.value('imagesettings', self.defaults)
        self.currentW = leftView.geometry().width()
        self.currentH = leftView.geometry().height()
        if applied:
            self.applied.connect(applied)
        # default browse directory (last used or user home):
        try:
            directory = self.settings.value('lastdir')
        except:
            directory = qtc.QDir.homePath()
        self.filename, ext = qtw.QFileDialog.getSaveFileName(
            self,
            'Save Image as...',
            directory,
            'PNG (*.png);;JPG (*.jpg);;BMP (*.bmp)',
            options=qtw.QFileDialog.DontUseNativeDialog
            )
        if self.filename:
            self.settings.setValue('lastdir', qtc.QFileInfo(self.filename).absolutePath())
            # extract file extension from selected filter:
            self.fileExt = re.search('\((.*?)\)', ext)[0][2:-1]
            if ext == 'PNG (*.png)':
                self.format = 'PNG'
            elif ext == 'JPG (*.jpg)':
                self.format = 'JPG'
            elif ext == 'BMP (*.bmp)':
                self.format = 'BMP'
            else:
                self.format = 'png'
            # remove extra extensions:
            self.filename = path.splitext(self.filename)[0]
            self.filename = self.filename+self.fileExt
            self.setFixedSize(qtc.QSize(500,580))
            self.setWindowTitle('Save Image Settings')
            buttonSize = qtc.QSize(110,35)
            layout = qtw.QVBoxLayout()
            self.setLayout(layout)
            self.tabs = qtw.QTabWidget()
            # references to subsettings:
            self.ist = ImageSettingsTab(self.currentW,self.currentH,self.format)
            self.ipt = ImagePreviewTab()
            self.tabs.addTab(self.ist, 'Settings')
            self.tabs.addTab(self.ipt, 'Preview')
            self.buttonbox = qtw.QWidget()
            self.buttonbox.setFixedHeight(60)
            self.cancelButton = qtw.QPushButton('Cancel', clicked=self.on_close)
            self.applyButton = qtw.QPushButton('OK', clicked=self.on_apply)
            self.applyButton.setDefault(True)
            self.cancelButton.setFixedSize(buttonSize)
            self.applyButton.setFixedSize(buttonSize)
            buttonlayout = qtw.QHBoxLayout()
            self.buttonbox.setLayout(buttonlayout)
            buttonlayout.setAlignment(qtc.Qt.AlignRight)
            buttonlayout.addWidget(self.cancelButton)
            buttonlayout.addWidget(self.applyButton)
            # add all the widgets to layout:
            layout.addWidget(self.tabs)
            layout.addWidget(self.buttonbox)
            # load presets&generated first preview:
            self.recover_settings()
            self.on_update()
            # signals:
            self.ist.changed.connect(self.on_update)
            # End main UI code
            self.show()
        else:
            self.parent.statusbar.showMessage('image save cancelled')
            
    def keyPressEvent(self, qKeyEvent):
        # Note: This is a workaround, because default buttons apparently only work in children of QDialog et al.
        if qKeyEvent.key() == qtc.Qt.Key_Return: 
            self.on_apply()
        else:
            super().keyPressEvent(qKeyEvent)            

    def recover_settings(self):
        rbuttons = self.ist.findChildren(qtw.QRadioButton)
        rcheck = self.ist.findChildren(qtw.QCheckBox)
        for item in rbuttons:
            if item.text() in self.data.get('checkedRadiobuttons'):
                item.setChecked(True)
        for item in rcheck:
            if item.text() in self.data.get('checkedCheckbuttons'):
                item.setChecked(True)
        self.ist.sbMultiple.setValue(self.data.get('customMultiplier'))
        self.ist.customRes.leCustomW.setText(str(self.data.get('customWidth')))
        self.ist.customRes.leCustomH.setText(str(self.data.get('customHeight')))
        
    def on_apply(self):
        self.get_currentData()
        self.settings.setValue('imagesettings', self.data)
        self.write_image()
        self.submitted.emit()
        self.close()

    def update_preview(self):
        self.ipt.setImage(self.image)

    def get_currentData(self):
        # collect data from widgets:
        rbuttons = self.ist.findChildren(qtw.QRadioButton)
        rcheck = self.ist.findChildren(qtw.QCheckBox)
        rbuttonlist = []
        checkblist = []
        for item in rbuttons:
            if item.isChecked():
                rbuttonlist.append(item.text())
        for item in rcheck:
            if item.isChecked():
                checkblist.append(item.text())
        self.data = {
            'checkedRadiobuttons' : rbuttonlist,
            'checkedCheckbuttons' : checkblist,
            'customWidth': int(self.ist.customRes.leCustomW.text()),
            'customHeight' : int(self.ist.customRes.leCustomH.text()),
            'customMultiplier' : self.ist.sbMultiple.value()            
        }    

    def on_update(self):
        self.get_currentData()
        # determine output size:
        imageW = int(self.currentW)
        imageH = int(self.currentH)            
        #elif 'Multiple of Current Size:' in self.data.get('checkedRadiobuttons'):         
        input_size = qtc.QSize(imageW,imageH)
        if 'Multiple of Current Size:' in self.data.get('checkedRadiobuttons'):
            multiplier = self.data.get('customMultiplier')
            imageW = int(imageW * multiplier)
            imageH = int(imageH * multiplier)
        elif '640 x 480 px' in self.data.get('checkedRadiobuttons'):
            imageW = 640
            imageH = 480
        elif '800 x 600 px' in self.data.get('checkedRadiobuttons'):
            imageW = 800
            imageH = 600
        elif '1024 x 768 px' in self.data.get('checkedRadiobuttons'):
            imageW = 1024
            imageH = 768
        elif 'Custom Size:' in self.data.get('checkedRadiobuttons'):
            imageW = int(self.data.get('customWidth'))
            imageH = int(self.data.get('customHeight'))
        # canvas setup:
        output_size = qtc.QSize(imageW,imageH)
        double_output_size = qtc.QSize(imageW,2*imageH)
        input_rect = qtc.QRect(qtc.QPoint(0,0),qtc.QSize(input_size))
        output_rect = qtc.QRectF(qtc.QPointF(0,0),qtc.QSizeF(output_size))
        double_output_rect = qtc.QRectF(qtc.QPointF(0,0),qtc.QSizeF(double_output_size))
        # pixmap definition:
        painter = qtg.QPainter()
        if 'Both Plots' in self.data.get('checkedRadiobuttons'):
            image = qtg.QPixmap(double_output_size)
        else:
            image = qtg.QPixmap(output_size)
        if 'Transparent Background' in self.data.get('checkedCheckbuttons') and self.format in ['PNG']:
            image.fill(qtc.Qt.transparent)
        else:
            if 'Grayscale' in self.data.get('checkedCheckbuttons'):
                image.fill(qtc.Qt.white)
            else:
                image.fill(self.backgroundColor)
        painter.begin(image)
        painter.setRenderHint(qtg.QPainter.Antialiasing)
        greyscale_eff1 = qtw.QGraphicsColorizeEffect()
        greyscale_eff2 = qtw.QGraphicsColorizeEffect()
        if 'Grayscale' in self.data.get('checkedCheckbuttons'):
            greyscale_eff1.setColor(qtg.QColor('#3d3d3d'))
            greyscale_eff2.setColor(qtg.QColor('#3d3d3d'))
            greyscale_eff1.setEnabled(True)
            greyscale_eff2.setEnabled(True)
            self.leftView.chart.setGraphicsEffect(greyscale_eff1)
            self.rightView.chart.setGraphicsEffect(greyscale_eff2)
        if 'DSC/Time Plot' in self.data.get('checkedRadiobuttons'):
            if 'Transparent Background' in self.data.get('checkedCheckbuttons') and self.format in ['PNG']:
                self.leftView.chart.setBackgroundBrush(qtg.QBrush(qtc.Qt.transparent))
            self.leftView.view.render(painter,
                                 source=input_rect,
                                 target=output_rect,
                                 mode=qtc.Qt.KeepAspectRatio)
        elif 'DSC/Temp Plot' in self.data.get('checkedRadiobuttons'):
            if 'Transparent Background' in self.data.get('checkedCheckbuttons') and self.format in ['PNG']:
                self.rightView.chart.setBackgroundBrush(qtg.QBrush(qtc.Qt.transparent))
            self.rightView.view.render(painter,
                                  source=input_rect,
                                  target=output_rect,
                                  mode=qtc.Qt.KeepAspectRatio)
        else:
            # both plots, just double the height:
            topimg = qtg.QPixmap(output_size)
            bottomimg = qtg.QPixmap(output_size)
            if 'Transparent Background' in self.data.get('checkedCheckbuttons') and self.format in ['PNG']:
                self.leftView.chart.setBackgroundBrush(qtg.QBrush(qtc.Qt.transparent))
                self.rightView.chart.setBackgroundBrush(qtg.QBrush(qtc.Qt.transparent))
                topimg.fill(qtc.Qt.transparent)
                bottomimg.fill(qtc.Qt.transparent)
            else:
                if 'Grayscale' in self.data.get('checkedCheckbuttons'):
                    image.fill(qtc.Qt.white)
                    topimg.fill(qtc.Qt.white)
                    bottomimg.fill(qtc.Qt.white)
                else:
                    image.fill(self.backgroundColor)
                    topimg.fill(self.backgroundColor)
                    bottomimg.fill(self.backgroundColor)
            toppainter = qtg.QPainter()
            bottompainter = qtg.QPainter()
            toppainter.begin(topimg)
            toppainter.setBackgroundMode(qtc.Qt.TransparentMode)
            toppainter.setRenderHint(qtg.QPainter.Antialiasing)
            self.leftView.view.render(toppainter,
                                source=input_rect,
                                target=output_rect,
                                mode=qtc.Qt.KeepAspectRatio)
            toppainter.end()
            bottompainter.begin(bottomimg)
            bottompainter.setBackgroundMode(qtc.Qt.TransparentMode)
            bottompainter.setRenderHint(qtg.QPainter.Antialiasing)
            self.rightView.view.render(bottompainter,
                                    source=input_rect,
                                    target=output_rect,
                                    mode=qtc.Qt.KeepAspectRatio)
            bottompainter.end()                                
            top_rect = qtc.QRectF(0,0,imageW,imageH)
            bottom_rect = qtc.QRectF(0,imageH,imageW,imageH)
            painter.drawPixmap(top_rect, topimg, qtc.QRectF(topimg.rect()))
            painter.drawPixmap(bottom_rect, bottomimg, qtc.QRectF(bottomimg.rect()))
        painter.end()
        greyscale_eff1.setEnabled(False)
        greyscale_eff2.setEnabled(False)
        self.leftView.chart.setBackgroundBrush(qtg.QBrush(self.backgroundColor))
        self.rightView.chart.setBackgroundBrush(qtg.QBrush(self.backgroundColor))
        self.output_rect.setSize(output_rect.size())
        self.double_output_rect.setSize(double_output_rect.size())
        self.image = image
        self.update_preview()

    def write_image(self):
        if 'Both Plots' in self.data.get('checkedRadiobuttons'):
            self.image.scaled(self.output_rect.width(), self.double_output_rect.height()).save(self.filename)
        else:
            self.image.scaled(self.output_rect.width(), self.output_rect.height()).save(self.filename)
        statusString = (
            f'{self.filename} : saved ('
            f'{self.format} format)'
            )
        self.parent.statusbar.showMessage(statusString)
            
    def on_close(self):
        self.close()
