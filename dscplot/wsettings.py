from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

from .auxiliary import getSettings


class FontButton(qtw.QPushButton):

    changed = qtc.pyqtSignal()

    def __init__(self, default_family, default_pointsize, changed=None):
        super().__init__()
        self.set_font(qtg.QFont(default_family, default_pointsize))
        self.clicked.connect(self.on_click)
        self.setFixedSize(qtc.QSize(300,50))
        if changed:
            self.changed.connect(changed)

    def set_font(self, font):
        self._font = font
        self.setFont(font)
        self.setText(f'{font.family()} {font.pointSize()}')

    def on_click(self):
        font, accepted = qtw.QFontDialog.getFont(self._font)
        if accepted:
            self.set_font(font)
            self.changed.emit()


class ColorButton(qtw.QPushButton):

    changed = qtc.pyqtSignal()

    def __init__(self, default_color, changed=None):
        super().__init__()
        self.setFixedSize(qtc.QSize(35,35))
        self.set_color(qtg.QColor(default_color))
        self.clicked.connect(self.on_click)
        if changed:
            self.changed.connect(changed)

    def set_color(self, color):
        self._color = color
        # update icon
        pixmap = qtg.QPixmap(32, 32)
        pixmap.fill(self._color)
        self.setIcon(qtg.QIcon(pixmap))

    def on_click(self):
        color = qtw.QColorDialog.getColor(self._color)
        if color:
            self.set_color(color)
            self.changed.emit()


class FontSettingsTab(qtw.QWidget):

    changed = qtc.pyqtSignal(dict)

    def __init__(self, changed=None):                    
        super().__init__()
        getSettings(self)
        if changed:
            self.changed.connect(changed)
        self.setFixedSize(qtc.QSize(450,330))
        buttonSize = qtc.QSize(110,35)
        defaultsButton = qtw.QPushButton('Defaults', clicked=self.get_defaults)
        defaultsButton.setFixedSize(buttonSize)
        self.textFontButton = FontButton(self.textFont.family(),self.textFont.pointSize(),
                                        changed=self.on_change)
        self.titleFontButton = FontButton(self.titleFont.family(),self.titleFont.pointSize(),
                                        changed=self.on_change)
        self.axisFontButton = FontButton(self.axisFont.family(),self.axisFont.pointSize(),
                                        changed=self.on_change)
        layout = qtw.QGridLayout()
        self.setLayout(layout)
        layout.addWidget(qtw.QLabel('Text Font:'),0,0)
        layout.addWidget(qtw.QLabel('Title Font:'),1,0)
        layout.addWidget(qtw.QLabel('Axis Font:'),2,0)
        layout.addWidget(self.textFontButton,0,1)
        layout.addWidget(self.titleFontButton,1,1)
        layout.addWidget(self.axisFontButton,2,1)
        layout.addWidget(defaultsButton, 3, 1, alignment=qtc.Qt.AlignRight)
        self.update_data()

    def update_data(self):
        self.data = {
            'titleFont': self.titleFontButton.font(),
            'textFont': self.textFontButton.font(),
            'axisFont': self.axisFontButton.font()
        }

    def on_change(self):
        self.update_data()
        self.changed.emit(self.data)

    def get_defaults(self):
        self.titleFontButton.set_font(qtg.QFont("Helvetica", 16))
        self.textFontButton.set_font(qtg.QFont("Helvetica", 10))
        self.axisFontButton.set_font(qtg.QFont("Helvetica", 10))
        self.update_data()
        self.changed.emit(self.data)
        

class ColorsSettingsTab(qtw.QWidget):

    changed = qtc.pyqtSignal(dict)

    def __init__(self, changed=None):                    
        super().__init__()
        getSettings(self)
        if changed:
            self.changed.connect(changed)
        self.setFixedSize(qtc.QSize(450,330))
        buttonSize = qtc.QSize(110,35)
        defaultsButton = qtw.QPushButton('Defaults', clicked=self.get_defaults)
        defaultsButton.setFixedSize(buttonSize)
        self.backgroundButton = ColorButton(self.backgroundColor, changed=self.on_change)
        self.tButton = ColorButton(self.tempColor, changed=self.on_change)
        self.dscButton = ColorButton(self.dscColor, changed=self.on_change)
        self.tascButton = ColorButton(self.tascColor, changed=self.on_change)
        self.axlabelButton = ColorButton(self.axisColor, changed=self.on_change)
        layout = qtw.QGridLayout()
        self.setLayout(layout)
        layout.addWidget(qtw.QLabel('Background:'),0,0)
        layout.addWidget(qtw.QLabel('T curve:'),1,0)
        layout.addWidget(qtw.QLabel('DSC Curve:'),2,0)
        layout.addWidget(qtw.QLabel('TASC Curve:'),3,0)
        layout.addWidget(qtw.QLabel('Titles & Labels:'),0,2)
        layout.addWidget(self.backgroundButton,0,1)
        layout.addWidget(self.tButton,1,1)
        layout.addWidget(self.dscButton,2,1)
        layout.addWidget(self.tascButton,3,1)
        layout.addWidget(self.axlabelButton,0,3)
        layout.addWidget(defaultsButton, 5, 3, alignment=qtc.Qt.AlignRight)
        self.update_data()

    def update_data(self):
        self.data = {
            'backgroundColor': self.backgroundButton._color,
            'axisColor': self.axlabelButton._color,
            'tempColor': self.tButton._color,
            'dscColor': self.dscButton._color,
            'tascColor': self.tascButton._color
        }

    def on_change(self):
        self.update_data()
        self.changed.emit(self.data)

    def get_defaults(self):
        self.backgroundButton.set_color(qtg.QColor(qtc.Qt.white))
        self.tButton.set_color(qtg.QColor(qtc.Qt.red))
        self.dscButton.set_color(qtg.QColor('#ff9900'))
        self.tascButton.set_color(qtg.QColor(qtc.Qt.blue))
        self.axlabelButton.set_color(qtg.QColor(qtc.Qt.black))
        self.update_data()
        self.changed.emit(self.data)
        

class AutoColorSettingsTab(qtw.QWidget):

    changed = qtc.pyqtSignal(dict)

    def __init__(self, changed=None):                    
        super().__init__()
        getSettings(self)
        if changed:
            self.changed.connect(changed)
        self.setFixedSize(qtc.QSize(450,330))
        buttonSize = qtc.QSize(110,35)
        defaultsButton = qtw.QPushButton('Defaults', clicked=self.get_defaults)
        defaultsButton.setFixedSize(buttonSize)
        self.warm1Button = ColorButton(self.warmColor1, changed=self.on_change)
        self.warm2Button = ColorButton(self.warmColor2, changed=self.on_change)
        self.warm3Button = ColorButton(self.warmColor3, changed=self.on_change)
        self.warm4Button = ColorButton(self.warmColor4, changed=self.on_change)
        self.cool1Button = ColorButton(self.coolColor1, changed=self.on_change)
        self.cool2Button = ColorButton(self.coolColor2, changed=self.on_change)
        self.cool3Button = ColorButton(self.coolColor3, changed=self.on_change)
        self.cool4Button = ColorButton(self.coolColor4, changed=self.on_change)
        layout = qtw.QGridLayout()
        self.setLayout(layout)
        layout.addWidget(qtw.QLabel('Warm Color #1:'),0,0)
        layout.addWidget(qtw.QLabel('Warm Color #2:'),1,0)
        layout.addWidget(qtw.QLabel('Warm Color #3:'),2,0)
        layout.addWidget(qtw.QLabel('Warm Color #4:'),3,0)
        layout.addWidget(qtw.QLabel('Cool Color #1:'),0,2)
        layout.addWidget(qtw.QLabel('Cool Color #2:'),1,2)
        layout.addWidget(qtw.QLabel('Cool Color #3:'),2,2)
        layout.addWidget(qtw.QLabel('Cool Color #4:'),3,2)
        layout.addWidget(self.warm1Button,0,1)
        layout.addWidget(self.warm2Button,1,1)
        layout.addWidget(self.warm3Button,2,1)
        layout.addWidget(self.warm4Button,3,1)
        layout.addWidget(self.cool1Button,0,3)
        layout.addWidget(self.cool2Button,1,3)
        layout.addWidget(self.cool3Button,2,3)
        layout.addWidget(self.cool4Button,3,3)
        layout.addWidget(defaultsButton, 5, 3, alignment=qtc.Qt.AlignRight)
        self.update_data()

    def update_data(self):
        self.data = {
            'warmColor1': self.warm1Button._color,
            'warmColor2': self.warm2Button._color,
            'warmColor3': self.warm3Button._color,
            'warmColor4': self.warm4Button._color,
            'coolColor1': self.cool1Button._color,
            'coolColor2': self.cool2Button._color,
            'coolColor3': self.cool3Button._color,
            'coolColor4': self.cool4Button._color
        }

    def on_change(self):
        self.update_data()
        self.changed.emit(self.data)

    def get_defaults(self):
        self.warm1Button.set_color(qtg.QColor('#8b0000'))
        self.warm2Button.set_color(qtg.QColor('#cd0000'))
        self.warm3Button.set_color(qtg.QColor('#ff0000'))
        self.warm4Button.set_color(qtg.QColor('#ff6347'))
        self.cool1Button.set_color(qtg.QColor('#00008b'))
        self.cool2Button.set_color(qtg.QColor('#0000cd'))
        self.cool3Button.set_color(qtg.QColor('#4169e1'))
        self.cool4Button.set_color(qtg.QColor('#87cefa'))
        self.update_data()
        self.changed.emit(self.data)


class SettingsWindow(qtw.QWidget):             

    applied = qtc.pyqtSignal()

    def __init__(self, parent, applied=None):                    
        super().__init__()
        # handle exit condition:
        parent.destroyed.connect(self.close)
        # recover settings:
        getSettings(self)
        if applied:
            self.applied.connect(applied)
        self.setFixedSize(qtc.QSize(500,450))
        self.setWindowTitle('DSCPlot Settings')
        buttonSize = qtc.QSize(110,35)
        layout = qtw.QVBoxLayout()
        self.setLayout(layout)
        self.tabs = qtw.QTabWidget()
        # references to subsettings:
        self.fs = FontSettingsTab()
        self.cs = ColorsSettingsTab()
        self.acs = AutoColorSettingsTab()
        self.tabs.addTab(self.fs, 'Fonts')
        self.tabs.addTab(self.cs, 'Colors')
        self.tabs.addTab(self.acs, 'Auto-Color')
        self.buttonbox = qtw.QWidget()
        self.buttonbox.setFixedHeight(60)
        self.cancelButton = qtw.QPushButton('Cancel', clicked=self.on_close)
        self.applyButton = qtw.QPushButton('OK', clicked=self.on_apply)
        self.applyButton.setDefault(True)
        self.cancelButton.setFixedSize(buttonSize)
        self.applyButton.setFixedSize(buttonSize)
        buttonlayout = qtw.QHBoxLayout()
        self.buttonbox.setLayout(buttonlayout)
        buttonlayout.setAlignment(qtc.Qt.AlignRight)
        buttonlayout.addWidget(self.cancelButton)
        buttonlayout.addWidget(self.applyButton)
        # add all the widgets to layout:
        layout.addWidget(self.tabs)
        layout.addWidget(self.buttonbox)
        # End main UI code
        self.show()
        
    def keyPressEvent(self, qKeyEvent):
        if qKeyEvent.key() == qtc.Qt.Key_Return: 
            self.on_apply()
        else:
            super().keyPressEvent(qKeyEvent)  

    def on_apply(self):
        self.settings.setValue('backgroundColor', self.cs.data.get('backgroundColor'))
        self.settings.setValue('fontColor', self.cs.data.get('fontColor'))
        self.settings.setValue('axisColor', self.cs.data.get('axisColor'))
        self.settings.setValue('tempColor', self.cs.data.get('tempColor'))
        self.settings.setValue('dscColor', self.cs.data.get('dscColor'))
        self.settings.setValue('tascColor', self.cs.data.get('tascColor'))
        self.settings.setValue('warmColor1', self.acs.data.get('warmColor1'))
        self.settings.setValue('warmColor2', self.acs.data.get('warmColor2'))
        self.settings.setValue('warmColor3', self.acs.data.get('warmColor3'))
        self.settings.setValue('warmColor4', self.acs.data.get('warmColor4'))
        self.settings.setValue('coolColor1', self.acs.data.get('coolColor1'))
        self.settings.setValue('coolColor2', self.acs.data.get('coolColor2'))
        self.settings.setValue('coolColor3', self.acs.data.get('coolColor3'))
        self.settings.setValue('coolColor4', self.acs.data.get('coolColor4'))
        self.settings.setValue('titleFont', self.fs.data.get('titleFont').toString())  
        self.settings.setValue('textFont', self.fs.data.get('textFont').toString())  
        self.settings.setValue('axisFont', self.fs.data.get('axisFont').toString())   
        self.applied.emit()
        self.close()

    def on_close(self):
        self.close()
