from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc

def getSettings(self):
    self.settings = qtc.QSettings('SMoCC','DSCPlot')
    ### get user settings / set defaults:
    # toggles:
    self.kelvin = self.settings.value('kelvin', False, type=bool)
    self.smooth = self.settings.value('smooth', True, type=bool)
    # colors:
    self.backgroundColor = self.settings.value('backgroundColor', qtg.QColor(qtc.Qt.white))
    self.fontColor = self.settings.value('fontColor', qtg.QColor(qtc.Qt.black))
    self.axisColor = self.settings.value('axisColor', qtg.QColor(qtc.Qt.black))
    self.tempColor = self.settings.value('tempColor', qtg.QColor(qtc.Qt.red))
    self.dscColor = self.settings.value('dscColor', qtg.QColor('#ff9900'))
    self.tascColor = self.settings.value('tascColor', qtg.QColor(qtc.Qt.blue))
    # auto-colors:
    self.warmColor1 = self.settings.value('warmColor1', qtg.QColor('#8b0000'))
    self.warmColor2 = self.settings.value('warmColor2', qtg.QColor('#cd0000'))
    self.warmColor3 = self.settings.value('warmColor3', qtg.QColor('#ff0000'))
    self.warmColor4 = self.settings.value('warmColor4', qtg.QColor('#ff6347'))
    self.coolColor1 = self.settings.value('coolColor1', qtg.QColor('#00008b'))
    self.coolColor2 = self.settings.value('coolColor2', qtg.QColor('#0000cd'))
    self.coolColor3 = self.settings.value('coolColor3', qtg.QColor('#4169e1'))
    self.coolColor4 = self.settings.value('coolColor4', qtg.QColor('#87cefa'))
    # fonts (use toString/fromString method on text object to store/recover):
    self.titleFont = qtg.QFont()
    self.titleFont.fromString(self.settings.value('titleFont', qtg.QFont("Helvetica", 16).toString()))
    self.textFont = qtg.QFont()
    self.textFont.fromString(self.settings.value('textFont', qtg.QFont("Helvetica", 10).toString()))
    self.axisFont = qtg.QFont()
    self.axisFont.fromString(self.settings.value('axisFont', qtg.QFont("Helvetica", 10).toString()))
        
def setSettings(self):
    """ This recycles all settings, for simple toggles and such better use separate
    self.settings.setValue calls. """
    # toggles:
    self.settings.setValue('kelvin', self.kelvin);
    self.settings.setValue('smooth', self.smooth)
    # colors:
    self.settings.setValue('backgroundColor', self.backgroundColor)
    self.settings.setValue('fontColor', self.fontColor)
    self.settings.setValue('axisColor', self.axisColor)
    self.settings.setValue('tempColor', self.tempColor)
    self.settings.setValue('dscColor', self.dscColor)
    self.settings.setValue('tascColor', self.tascColor)
    # auto-colors:
    self.settings.setValue('warmColor1', self.warmColor1)
    self.settings.setValue('warmColor2', self.warmColor2)
    self.settings.setValue('warmColor3', self.warmColor3)
    self.settings.setValue('warmColor4', self.warmColor4)
    self.settings.setValue('coolColor1', self.coolColor1)
    self.settings.setValue('coolColor2', self.coolColor2)
    self.settings.setValue('coolColor3', self.coolColor3)
    self.settings.setValue('coolColor4', self.coolColor4)
    # fonts (use toString/fromString method on text object to store/recover):
    self.settings.setValue('titleFont', self.titleFont.toString())  
    self.settings.setValue('textFont', self.textFont.toString())   
    self.settings.setValue('axisFont', self.axisFont.toString())      
