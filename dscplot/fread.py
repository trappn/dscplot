import sys
import re
from os import path
import csv

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc

class Reader(qtw.QWidget):
    """ Read raw, detect format, reformat, send data to main. """
    
    def __init__(self):
        super().__init__()
        settings = qtc.QSettings('SMoCC','DSCPlot')
        # default browse directory (last used or user home):
        try:
            directory = settings.value('lastdir')
        except:
            directory = qtc.QDir.homePath()
        self.filename, _ = qtw.QFileDialog.getOpenFileName(
            self,
            'Open File',
            directory,
            'All Files (*);;DSCPlot Project (*.dsc);;ASCII File - LINK/DSCPlot (*.txt);;CSV Spreadsheet - dscplot (*.csv)',
            options=qtw.QFileDialog.DontUseNativeDialog
            )
        if self.filename:
            settings.setValue('lastdir', qtc.QFileInfo(self.filename).absolutePath())
            self.importFile()
        else:
            self.format = 'cancelled'
            
    def importFile(self):
        self.data = []
        metalst = []
        self.notes = ''
        self.meta = ''
        self.format = 'unknown'
        
        with open(self.filename, 'r') as fh:
            try:
                fl = fh.readlines()
                firstline = fl[0].strip().replace(' ','').replace('\t', '').replace('\n','')
                secondline = fl[1].strip().replace(' ','').replace('\t', '').replace('\n','')
                counter = 0
                self.nheaderlines = 0
                for index, line in enumerate(fl):
                    # detect beginning/end of all data types (DSCPlot format):
                    if line.rstrip() == '<NOTES>':
                        notesstartindex = index+1                    
                    if line.rstrip() == '</NOTES>':
                        notesendindex = index  
                    if line.rstrip() == '<META>':
                        metastartindex = index+1                    
                    if line.rstrip() == '</META>':
                        metaendindex = index  
                    if line.rstrip() == '<COLUMNS>':
                        columnsindex = index+1
                    if line.rstrip() == '<DATA>':
                        datastartindex = index+1
                    # detect if data was resampled:
                    for word in ['New', 'newIndex', 'Index']:
                        if word in line.split(maxsplit=2) or word in line.split(';', 2):
                            if word in ['New', 'newIndex']:
                                self.included_cols = [0, 1, 2, 3, 11, 22, 23]
                                self.column_header = ['newIndex','origIndex','t[s]','T[°C]','DSC','TASC_x[s]','TASC_y']
                            else:
                                self.included_cols = [0, 1, 2, 10, 21, 22]
                                self.column_header = ['Index','t[s]','T[°C]','DSC','TASC_x[s]','TASC_y']
                            self.nheaderlines = counter + 1           
                            break
                    counter += 1
                    
                    
                # try to detect format:
                if firstline in [ 'Index;t[s];T[°C];DSC;TASC_x[s];TASC_y',
                                  'newIndex;origIndex;t[s];T[°C];DSC;TASC_x[s];TASC_y' ]:
                    self.format = 'DSCPlot CSV'
                    self.reader = csv.reader(fl, delimiter=';')
                    for index, row in enumerate(self.reader):
                        if index > 0:
                            try:
                                self.data.append(row)
                                print(row)
                            except:
                                pass
                    self.meta = 'Metadata stripped by DSCPlot conversion.'

                elif firstline in [ 'Indext[s]T[°C]DSCTASC_x[s]TASC_y',
                                    'newIndexorigIndext[s]T[°C]DSCTASC_x[s]TASC_y' ]:
                    self.format = 'DSCPlot TXT'
                    self.reader = csv.reader(fl, delimiter='\t')
                    for index, row in enumerate(self.reader):
                        if index > 0:
                            try:
                                self.data.append(row)
                                print(row)
                            except:
                                pass
                    self.meta = 'Metadata stripped by DSCPlot conversion.'

                elif firstline in ['Data:']:
                    self.format = 'Linkam TXT'
                    self.reader = csv.reader(fl, delimiter='\t')
                    for index, row in enumerate(self.reader):
                        if index >= self.nheaderlines - 1:
                            break
                        metalst.append(row)
                    #convert metadata to string:
                    metaout = ''
                    for item, txt in enumerate(metalst):
                        # strip leading & trailing empty lines:
                        if 2 <= item <= len(metalst) - 5:
                            # strip empty fields:
                            metaout = metaout + ' '.join(txt) + '\n'
                    self.meta = str(metaout.rstrip())
                    for row in self.reader:
                        try:
                            filtered_content = [row[i] for i in self.included_cols]
                            filtered_content = [re.sub('[^0-9\.\-]', '', s) for s in filtered_content]
                            for s in range(len(filtered_content)):
                                if s>0:
                                    filtered_content[s] = "{:.3f}".format(float(filtered_content[s]))
                        except:
                            pass
                        self.data.append(filtered_content)
                    try:
                        # TASC data should be placed at correct lines with respect to DSC time:
                        tascfirstval = float(self.data[0][len(self.included_cols)-2])
                        if tascfirstval:
                            # split off TASCx/y & kick off empties:
                            tasclst = [ [i[len(self.included_cols)-2],i[len(self.included_cols)-1]] for i in self.data ]
                            tasclst = [x for x in tasclst if x[0]]
                            # now shift tascx / tascy to the new index (corresponding to closest time value):
                            counter = 0
                            for i in tasclst:
                                for num, j in enumerate(self.data):
                                    if num >= counter:
                                        if float(i[0]) >= float(j[len(self.included_cols)-5]):
                                            j[len(self.included_cols)-2] = ''
                                            j[len(self.included_cols)-1] = ''
                                        else:
                                            j[len(self.included_cols)-2] = i[0]
                                            j[len(self.included_cols)-1] = i[1]
                                            counter = num + 1
                                            break
                    except:
                        pass
                    metastr = self.meta.replace('\n','')
                    markers = "Notes\:(.*?)Sample\ Calibration"
                    importstr = '<Imported from LINK>\n\n'
                    notes = re.findall(markers, metastr)
                    importstr += importstr.join(notes)
                    importstr += '\n\n</Imported from LINK>\n'
                    self.notes = importstr    
                elif secondline in ['DSCPlot']:
                    self.format = 'DSCPlot DSC'
                    self.reader = csv.reader(fl, delimiter='\t')
                    for index, row in enumerate(self.reader):
                        if index == columnsindex:
                            self.column_header = row                
                        if index >= datastartindex:
                            try:
                                self.data.append(row)
                                print(row)
                            except:
                                pass
                    del self.data[-1]
                    # recover notes and metadata as strings:
                    fh.seek(0)
                    fl = fh.readlines()
                    for index, line in enumerate(fl):
                        if index >= notesstartindex and index < notesendindex:
                            self.notes += line
                        if index >= metastartindex and index < metaendindex:
                            self.meta += line
                    fh.close()
                    self.notes = self.notes.rstrip()
                    self.meta = self.meta.rstrip()
            except:
                pass
                
                    
