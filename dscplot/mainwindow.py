import sys
import re
import copy
from os import path

from PyQt5 import QtWidgets as qtw
from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc
from PyQt5 import QtPrintSupport as qtps

from PyQt5 import QtChart as qtch

from .fread import Reader 
from .fexport import Writer
from .dtable import DataTableForm, CsvTableModel
from .dgraph import DscVsTimeView, DscVsTempView
from .wsettings import SettingsWindow
from .wprint import PrintSettingsWindow, MultiPdfSettingsWindow
from .wimage import ExportImageSettingsWindow
from .auxiliary import getSettings, setSettings

class MainWindow(qtw.QMainWindow):           

    VERSION = '1.0.1'

    def __init__(self):                     
        """MainWindow/GUI constructor"""
        super().__init__()
        ### get settings:
        getSettings(self)
        ### Make icons available:
        if getattr(sys, 'frozen', False):
            # compiled with PyInstaller:
            directory = sys._MEIPASS
        else:
            # run as python script:
            directory = path.dirname(__file__)
      
        ### Main UI code follows:

        # Global Attributes:
        self.setAttribute(qtc.Qt.WA_DeleteOnClose)
        self.setWindowTitle('DSCPlot  v' + self.VERSION)
        self.setMinimumSize(1000, 700);
        style = self.style()

        # File menu:
        self.file_menu = qtw.QMenu('&File', self)
        self.mi_open = self.file_menu.addAction('&Open',
                                                self.fileOpen,
                                                qtc.Qt.CTRL + qtc.Qt.Key_O)
        self.mi_revert = self.file_menu.addAction('&Revert',
                                                  self.revertData,
                                                  qtc.Qt.CTRL + qtc.Qt.Key_R)
        self.file_menu.addSeparator()
        self.mi_save = self.file_menu.addAction('&Save as...',
                                                self.fileSave,
                                                qtc.Qt.CTRL + qtc.Qt.Key_S)
        self.mi_eximage = self.file_menu.addAction('&Export Image as...',
                                                   self.imageSave,qtc.Qt.CTRL + qtc.Qt.Key_I)
        self.mi_expdf = self.file_menu.addAction('&Export PDF',
                                                 self.pdfDialog)
        self.mi_exmultipdf = self.file_menu.addAction('&Export multi-PDF',
                                                      self.multiPdfDialog)
        self.file_menu.addSeparator()
        self.mi_print = self.file_menu.addAction('&Print...',
                                                self.printDialog,
                                                qtc.Qt.CTRL + qtc.Qt.Key_P)
        self.file_menu.addSeparator()
        self.mi_quit = self.file_menu.addAction('&Quit',
                                                self.fileQuit,
                                                qtc.Qt.CTRL + qtc.Qt.Key_Q)
        self.mi_revert.setEnabled(False)   
        self.mi_save.setEnabled(False)   
        self.mi_eximage.setEnabled(False)                         
        self.mi_expdf.setEnabled(False)   
        self.mi_print.setEnabled(False)   
        self.menuBar().addMenu(self.file_menu)

        # Display menu:
        self.display_menu = qtw.QMenu('&Display', self)
        self.mi_toggleD = qtw.QAction('&Toggle DSC display',
                                      self,
                                      checkable=True)
        self.display_menu.addAction(self.mi_toggleD)
        self.mi_toggleT = qtw.QAction('&Toggle TASC display',
                                      self,
                                      checkable=True)
        self.display_menu.addAction(self.mi_toggleT)
        self.display_menu.addSeparator()  
        self.mi_toggleK = self.display_menu.addAction('&Toggle °C <-> K',
                                                      self.toggleK,
                                                      qtc.Qt.CTRL + qtc.Qt.Key_T)
        self.mi_toggleAutocolor = self.display_menu.addAction('&Auto-Color (DSC vs. T)',
                                                            self.about)
        self.display_menu.addSeparator()  
        self.mi_toggleG = qtw.QAction('&Show Grid',
                                      self,
                                      checkable=True)
        self.display_menu.addAction(self.mi_toggleG)
        self.mi_toggleTitle = qtw.QAction('&Show Titles',
                                      self,
                                      checkable=True)
        self.display_menu.addAction(self.mi_toggleTitle)
        self.mi_toggleLegend = qtw.QAction('&Show Legend',
                                      self,
                                      checkable=True)
        self.display_menu.addAction(self.mi_toggleLegend)        
        self.mi_toggleSplines = qtw.QAction('&Smooth Curves (splines)',
                                            self,
                                            checkable=True)
        self.display_menu.addAction(self.mi_toggleSplines)                                                    
        self.display_menu.addSeparator()
        self.mi_toggleToolbar = qtw.QAction('&Show Toolbar',
                                            self,
                                            checkable=True)
        self.display_menu.addAction(self.mi_toggleToolbar)  
        self.mi_settings = self.display_menu.addAction('&Settings...',
                                                       self.updateSettings)
        self.mi_toggleT.setEnabled(False)
        self.mi_toggleD.setEnabled(False)
        self.mi_toggleD.setChecked(True)        
        self.mi_toggleK.setEnabled(False)
        self.mi_toggleG.setEnabled(False)
        self.mi_toggleG.setChecked(True)
        self.mi_toggleTitle.setEnabled(False)
        self.mi_toggleTitle.setChecked(True)
        self.mi_toggleLegend.setEnabled(False)
        self.mi_toggleLegend.setChecked(True)
        self.mi_toggleAutocolor.setEnabled(False)
        self.mi_toggleSplines.setEnabled(False)
        self.mi_toggleSplines.setChecked(True)
        self.mi_toggleToolbar.setChecked(True)
        self.menuBar().addMenu(self.display_menu)                         

        # Tools menu:
        self.tools_menu = qtw.QMenu('&Tools', self)
        self.mi_crop = self.tools_menu.addAction('&Crop Data...',
                                                 self.about)
        self.mi_cropsel = self.tools_menu.addAction('&Crop Data (to Selection)',
                                                 self.about)
        self.mi_stripTasc = self.tools_menu.addAction('&Remove TASC Data',
                                                 self.about)                                         
        self.mi_reduce = self.tools_menu.addAction('&Downsample...',
                                                   self.about)
        self.mi_scan = self.tools_menu.addAction('&Scan',
                                                   self.about)  
        self.mi_peaks = self.tools_menu.addAction('&Find Peaks...',
                                                   self.about)
        self.mi_crop.setEnabled(False)
        self.mi_cropsel.setEnabled(False)          
        self.mi_reduce.setEnabled(False)
        self.mi_peaks.setEnabled(False)
        self.mi_scan.setEnabled(False)
        self.mi_stripTasc.setEnabled(False)
        self.menuBar().addMenu(self.tools_menu)

        # Help menu:
        self.help_menu = qtw.QMenu('&Help', self)
        self.menuBar().addMenu(self.help_menu)
        self.help_menu.addAction('&About', self.about)

        # File toolbar:
        self.file_tb = self.addToolBar('File')
        self.file_tb.setIconSize(qtc.QSize(50, 50))
        self.tb_open = self.file_tb.addAction('Open')
        self.tb_open.triggered.connect(self.fileOpen)
        self.tb_open.setIcon(qtg.QIcon(path.join(directory, 'icons', 'opened-folder-50.png')))
        self.tb_revert = self.file_tb.addAction('Revert')
        self.tb_revert.triggered.connect(self.revertData)
        self.tb_revert.setIcon(qtg.QIcon(path.join(directory, 'icons', 'restart-50.png')))
        self.tb_save = self.file_tb.addAction('Save')
        self.tb_save.triggered.connect(self.fileSave)
        self.tb_save.setIcon(qtg.QIcon(path.join(directory, 'icons', 'save-50.png')))
        self.tb_eximage = self.file_tb.addAction('Export Image')
        self.tb_eximage.triggered.connect(self.imageSave)
        self.tb_eximage.setIcon(qtg.QIcon(path.join(directory, 'icons', 'image-50.png')))
        self.tb_expdf = self.file_tb.addAction('Export PDF')
        self.tb_expdf.setIcon(qtg.QIcon(path.join(directory, 'icons', 'export-pdf-50.png')))
        self.tb_expdf.triggered.connect(self.pdfDialog)
        self.tb_print = self.file_tb.addAction('Print')
        self.tb_print.setIcon(qtg.QIcon(path.join(directory, 'icons', 'print-50.png')))
        self.tb_print.triggered.connect(self.printDialog)

        # Display toolbar:
        self.display_tb = self.addToolBar('Display')
        self.display_tb.setIconSize(qtc.QSize(50, 50))
        self.display_tb_pan = self.display_tb.addAction('Pan')
        self.display_tb_pan.setIcon(qtg.QIcon(path.join(directory, 'icons', 'drag-50.png')))
        self.display_tb_select = self.display_tb.addAction('Select')
        self.display_tb_select.setIcon(qtg.QIcon(path.join(directory, 'icons', 'selection-50.png')))
        self.display_tb_crop = self.display_tb.addAction('Crop to Selection')
        self.display_tb_crop.setIcon(qtg.QIcon(path.join(directory, 'icons', 'crop-selection-50.png')))
        self.display_tb_zoomin = self.display_tb.addAction('Zoom IN')
        self.display_tb_zoomin.setIcon(qtg.QIcon(path.join(directory, 'icons', 'zoom-in-50.png')))
        self.display_tb_zoomout = self.display_tb.addAction('Zoom OUT')
        self.display_tb_zoomout.setIcon(qtg.QIcon(path.join(directory, 'icons', 'zoom-out-50.png')))

        # Plot toolbar:
        self.plot_tb = self.addToolBar('Plot')
        self.plot_tb.setIconSize(qtc.QSize(50, 50))
        self.plot_tb_toggleK = self.plot_tb.addAction('Toggle °C/K')
        self.plot_tb_toggleK.triggered.connect(self.toggleK)
        self.plot_tb_toggleK.setIcon(qtg.QIcon(path.join(directory, 'icons', 'tc-toggle-50.png')))
        self.plot_tb_toggleTasc = self.plot_tb.addAction('Toggle TASC')
        self.plot_tb_toggleTasc.setIcon(qtg.QIcon(path.join(directory, 'icons', 'tasc-toggle-50.png')))
        self.plot_tb_toggleDsc = self.plot_tb.addAction('Toggle DSC')
        self.plot_tb_toggleDsc.setIcon(qtg.QIcon(path.join(directory, 'icons', 'dsc-toggle-50.png')))
        self.plot_tb_scan = self.plot_tb.addAction('Scan')
        self.plot_tb_scan.setIcon(qtg.QIcon(path.join(directory, 'icons', 'hunt-80.png')))        
        self.plot_tb_peakPick = self.plot_tb.addAction('Find Peaks')
        self.plot_tb_peakPick.setIcon(qtg.QIcon(path.join(directory, 'icons', 'peak-pick-50.png')))

        # initially invisible toolbar items:
        self.plot_tb_toggleTasc.setVisible(False)    
        
        # disable all toolbar items in the beginning except file open:
        for toolbar in self.findChildren(qtw.QToolBar):
            for action in toolbar.actions():
                if not action.text() == 'Open':
                    action.setEnabled(False)
        
        # Status Bar:
        self.statusbar = self.statusBar()
        self.statusbar.showMessage('', 1) 

        # Main Widget (Tabs & Definitions):
        self.tabs = qtw.QTabWidget()
        self.linkplot = DscVsTimeView()
        self.dscplot = DscVsTempView()
        self.datadisp = DataTableForm()
        self.metadata = qtw.QTextEdit(readOnly=True)
        self.notes = qtw.QTextEdit()
        self.tabs.addTab(self.linkplot, "DSC vs. t")
        self.tabs.addTab(self.dscplot, "DSC vs. T")
        self.tabs.addTab(self.datadisp, "Data")
        self.tabs.addTab(self.metadata, "Metadata")
        self.tabs.addTab(self.notes, "Notes")
        self.metadata.setFont(self.textFont)
        self.notes.setFont(self.textFont)
        self.setCentralWidget(self.tabs)

        # disable all tabs in the beginning:
        for tab in self.findChildren(qtw.QTabWidget):
            tab.setVisible(False)

        # Signals:
        self.datadisp.removed.connect(self.onTableChange)
        self.datadisp.restored.connect(self.revertData)
        self.mi_toggleT.triggered.connect(self.linkplot.toggleTasc)
        self.mi_toggleT.triggered.connect(self.dscplot.toggleTasc)
        self.plot_tb_toggleTasc.triggered.connect(self.linkplot.toggleTasc)
        self.plot_tb_toggleTasc.triggered.connect(self.dscplot.toggleTasc)
        self.plot_tb_toggleDsc.triggered.connect(self.linkplot.toggleDsc)
        self.plot_tb_toggleDsc.triggered.connect(self.dscplot.toggleDsc)
        self.mi_toggleD.triggered.connect(self.linkplot.toggleDsc)
        self.mi_toggleD.triggered.connect(self.dscplot.toggleDsc)
        self.mi_toggleG.triggered.connect(self.toggleGrid)
        self.mi_toggleTitle.triggered.connect(self.toggleTitle)
        self.mi_toggleLegend.triggered.connect(self.toggleLegend)
        self.mi_toggleSplines.triggered.connect(self.linkplot.toggleSplines)
        self.mi_toggleSplines.triggered.connect(self.dscplot.toggleSplines)
        self.mi_toggleToolbar.triggered.connect(self.toggleToolbar)
        self.linkplot.tascPresent.connect(lambda: self.mi_toggleT.setEnabled(True))
        self.linkplot.tascPresent.connect(lambda: self.mi_stripTasc.setEnabled(True))
        self.linkplot.tascPresent.connect(lambda: self.plot_tb_toggleTasc.setVisible(True))
        self.linkplot.updated.connect(self.updateMenu)
        self.linkplot.updated.connect(self.toggleTitle)
        self.dscplot.updated.connect(self.toggleTitle)
        
        # init printer:
        self.printer = qtps.QPrinter()
        
        ### End main UI code
        self.show()
        
    def fileOpen(self):
        self.currentData = Reader()
        if self.currentData.format not in ['unknown', 'cancelled']:
            self.rawData = copy.deepcopy(self.currentData.data)
            self.datadisp.model = CsvTableModel(
                self.currentData.data, self.currentData.column_header)
            self.mi_toggleT.setEnabled(False)
            self.mi_stripTasc.setEnabled(False)
            self.linkplot.tascShown = True
            self.dscplot.tascShown = True
            self.linkplot.dscShown = True
            self.dscplot.dscShown = True
            self.redrawGui()
            self.redrawLinkplot()
            self.redrawDsc()
            self.redrawTable()
            self.redrawMeta()
            self.redrawNotes()
            # enable all tabs:
            for tab in self.findChildren(qtw.QTabWidget):
                tab.setVisible(True)
        elif self.currentData.format == 'unknown':
            statusString = (
                f'{self.currentData.filename} : load failed'
                ' (unknown format)'
                )
            self.statusBar().showMessage(statusString)
            
    def fileSave(self):
        self.saveObject = Writer(self.currentData, self.notes.toPlainText(),self.VERSION)
        if self.saveObject.format not in ['failed', 'cancelled']:
            statusString = (
                f'{self.saveObject.filename} : saved ('
                f'{self.saveObject.format} format)'
                )
        elif self.saveObject.format == 'failed':
            statusString = (
                f'{self.saveObject.filename} : save failed'
                )
        else:
            statusString = 'file save cancelled'
        self.statusBar().showMessage(statusString)
        

    def fileQuit(self):
        self.close()
        
    def imageSave(self):
        # very dirty hack for image sizes not properly scaling
        # (apparently plots must be visible on screen at least once before render):
        previous = self.tabs.currentIndex()
        self.tabs.setCurrentIndex(0)
        self.tabs.setCurrentIndex(1)
        self.tabs.setCurrentIndex(previous)
        self.imageSaveObject = ExportImageSettingsWindow(self, self.linkplot, self.dscplot)

    def printDialog(self):
        # very dirty hack for image sizes not properly scaling
        # (apparently plots must be visible on screen at least once before render):
        previous = self.tabs.currentIndex()
        self.tabs.setCurrentIndex(0)
        self.tabs.setCurrentIndex(1)
        self.tabs.setCurrentIndex(previous)
        self.printObject = PrintSettingsWindow(self, self.linkplot, self.dscplot, self.printer, self.currentData)

    def pdfDialog(self):
        # very dirty hack for image sizes not properly scaling
        # (apparently plots must be visible on screen at least once before render):
        previous = self.tabs.currentIndex()
        self.tabs.setCurrentIndex(0)
        self.tabs.setCurrentIndex(1)
        self.tabs.setCurrentIndex(previous)
        self.printObject = PrintSettingsWindow(self, self.linkplot, self.dscplot, self.printer, self.currentData, pdf=True)

    def multiPdfDialog(self):
        self.printObject = MultiPdfSettingsWindow(self, self.printer)

    def revertData(self):
        self.currentData.data = copy.deepcopy(self.rawData)
        self.datadisp.model = CsvTableModel(
                self.currentData.data, self.currentData.column_header)
        self.redrawTable()
        self.redrawLinkplot()
        self.redrawDsc()
        
    def redrawGui(self):
        statusString = (
            f'{self.currentData.filename} : loaded ('
            f'{self.currentData.format} format)'
            )
        self.setWindowTitle(path.basename(self.currentData.filename)
                            + ' - '
                            + 'DSCPlot  v'
                            + self.VERSION
                            )
        self.statusBar().showMessage(statusString)
        # enable all toolbar items:
        for toolbar in self.findChildren(qtw.QToolBar):
            for action in toolbar.actions():
                action.setEnabled(True)
        # enable all menu items:
        for menu in self.findChildren(qtw.QMenu):
            for action in menu.actions():
                if not action.text() in ['&Toggle TASC display', '&Remove TASC Data']:
                    action.setEnabled(True)
        # enable table view buttons:
        for button in self.datadisp.findChildren(qtw.QPushButton):
            button.setEnabled(True)
        if self.linkplot.tascShown:
            self.mi_toggleT.setChecked(True)
        else:
            self.mi_toggleT.setChecked(False)
        if self.linkplot.splinesShown:
            self.mi_toggleSplines.setChecked(True)
        else:
            self.mi_toggleSplines.setChecked(False)
        if self.linkplot.dscShown:
            self.mi_toggleD.setChecked(True)
        else:
            self.mi_toggleD.setChecked(False)          

    def updateMenu(self):
        if self.linkplot.tascShown:
            self.mi_toggleT.setChecked(True)
        else:
            self.mi_toggleT.setChecked(False)        
        if self.linkplot.splinesShown:
            self.mi_toggleSplines.setChecked(True)
        else:
            self.mi_toggleSplines.setChecked(False) 
            
    def updateSettings(self):
        self.settingsWindow = SettingsWindow(self)
        self.settingsWindow.applied.connect(self.applySettings)

    def applySettings(self):
        getSettings(self)
        try:
            self.redrawLinkplot()
            self.redrawDsc()
            self.metadata.setFont(self.textFont)
            self.notes.setFont(self.textFont)
        except:
            self.linkplot.restyle()
            self.dscplot.restyle()
            self.metadata.setFont(self.textFont)
            self.notes.setFont(self.textFont)

    def redrawLinkplot(self):
        self.linkplot.refresh(self.currentData.data)
            
    def redrawDsc(self):
        self.dscplot.refresh(self.currentData.data)

    def redrawTable(self):
        self.datadisp.table.setModel(self.datadisp.model)

    def redrawNotes(self):
        self.notes.setText(self.currentData.notes)

    def redrawMeta(self):
        try:
            self.metadata.setText(str(self.currentData.meta))
        except:
            pass

    def onTableChange(self):
        self.currentData.data = self.datadisp.model.tableDataAsList()
        self.redrawLinkplot()
        self.redrawDsc()
        
    def toggleK(self):
        self.kelvin = not self.kelvin
        self.settings.setValue('kelvin', self.kelvin);
        self.redrawLinkplot()
        self.redrawDsc()
        self.redrawTable()

    def toggleToolbar(self):
        if self.mi_toggleToolbar.isChecked() == False:
            self.file_tb.setVisible(False)
            self.display_tb.setVisible(False)
            self.plot_tb.setVisible(False)
        else:
            self.file_tb.setVisible(True)
            self.display_tb.setVisible(True)
            self.plot_tb.setVisible(True)
        
    def toggleGrid(self):
        if self.mi_toggleG.isChecked():
            self.linkplot.y_axisT.setGridLineVisible(True)
            self.linkplot.x_axis.setGridLineVisible(True)
            self.dscplot.y_axis.setGridLineVisible(True)
            self.dscplot.x_axis.setGridLineVisible(True)
        else:
            self.linkplot.y_axisT.setGridLineVisible(False)
            self.linkplot.x_axis.setGridLineVisible(False)
            self.dscplot.y_axis.setGridLineVisible(False)
            self.dscplot.x_axis.setGridLineVisible(False)

    def toggleTitle(self):
        if self.mi_toggleTitle.isChecked():
            self.linkplot.chart.setTitle(self.linkplot.chart_title)
            self.dscplot.chart.setTitle(self.dscplot.chart_title)
        else:
            self.linkplot.chart.setTitle('')
            self.dscplot.chart.setTitle('')

    def toggleLegend(self):
        if self.mi_toggleLegend.isChecked():
            self.linkplot.chart.legend().setVisible(True)
            self.dscplot.chart.legend().setVisible(True)
        else:
            self.linkplot.chart.legend().setVisible(False)
            self.dscplot.chart.legend().setVisible(False)

    def about(self):
        qtw.QMessageBox.about(self, "About",
                              """DSCPlot v""" + self.VERSION + """

Simple plotting tool for DSC data measured
on Linkam DSC cells (e.g. DSC600).
Works on LINK data (export -> .txt).

(c) 2019-2020 Nils Trapp
Small Molecule Center @ ETH Zurich

Icons downloaded from https://icons8.com
""")
