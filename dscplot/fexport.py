import sys
import re
from os import path, remove
import csv
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc

class Writer(qtw.QWidget):
    """ Get data/metadata and save to formatted output file. """
    
    def __init__(self, dataobj, notes, version):
        super().__init__()
        settings = qtc.QSettings('SMoCC','DSCPlot')
        self.currentData = dataobj
        self.notes = notes
        self.version = version
        # default browse directory (last used or user home):
        try:
            directory = settings.value('lastdir')
        except:
            directory = qtc.QDir.homePath()
        self.filename, ext = qtw.QFileDialog.getSaveFileName(
            self,
            'Save File as...',
            directory,
            'DSCPlot Project (*.dsc);;ASCII File - TAB separated (*.txt);;CSV Spreadsheet (*.csv);;All Files (*)',
            options=qtw.QFileDialog.DontUseNativeDialog
            )
        if self.filename:
            settings.setValue('lastdir', qtc.QFileInfo(self.filename).absolutePath())
            # extract file extension from selected filter:
            self.fileExt = re.search('\((.*?)\)', ext)[0][2:-1]
            if ext == 'DSCPlot Project (*.dsc)':
                self.format = 'DSCPlot DSC'
            elif ext == 'ASCII File - TAB separated (*.txt)':
                self.format = 'DSCPlot TXT'
            elif ext == 'CSV Spreadsheet (*.csv)':
                self.format = 'DSCPlot CSV'
            else:
                self.format = 'DSCPlot TXT'
            # remove extra extensions:
            self.filename = path.splitext(self.filename)[0]
            self.filename = self.filename+self.fileExt
            self.exportFile()
        else:
            self.format = 'cancelled'
            
    def exportFile(self):
        with open(self.filename, 'w', newline='') as fh:
        #    try:
            if self.format == 'DSCPlot CSV':
                writer = csv.writer(fh, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            elif self.format == 'DSCPlot TXT':
                writer = csv.writer(fh, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            elif self.format == 'DSCPlot DSC':
                # own project format uses XML-style & TAB-separated ASCII for data (can be changed):
                # get manual / imported notes (if not empty or only whitespace):
                fh.write('<FORMAT>\nDSCPlot\nVERSION '+self.version+'\n</FORMAT>\n\n')
                if len("".join(self.notes.split())) > 0:
                    fh.write('<NOTES>\n'+self.notes.strip()+'\n</NOTES>\n\n')
                # same for metadata:
                if len("".join(self.currentData.meta.split())) > 0:
                    fh.write('<META>\n'+self.currentData.meta+'\n</META>\n\n')
                writer = csv.writer(fh, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            else:
                self.format = 'failed'
                return
            if self.format == 'DSCPlot DSC':
                fh.write('<COLUMNS>\n')
            writer.writerow(self.currentData.column_header)
            if self.format == 'DSCPlot DSC':
                fh.write('</COLUMNS>\n\n<DATA>\n')
            for item in self.currentData.data:
                writer.writerow(item)
            if self.format == 'DSCPlot DSC':
                fh.write('</DATA>')
            
        #    except:
        #        self.format = 'failed'         # REMEMBER TO REACTIVATE THIS WHEN IT WORKS!
        # clean up empty file if something went wrong above:
        #if self.format == 'failed':  # REMEMBER TO REACTIVATE THIS WHEN IT WORKS!
        #    remove(self.filename)