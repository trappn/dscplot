#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
run this from the main dir, e.g. python3 setup\make_mac_release.py
this script is FOR MAC OS ONLY !
"""
import subprocess
import os,sys,inspect
import shlex

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

from dscplot.mainwindow import MainWindow as MainWindow

# make PyInstaller dists:
cmd="python3 -m PyInstaller --noconfirm --clean --noconsole --name DSCPlot --icon=setup/dscplot.icns --add-data dscplot/icons:icons --windowed run.py"
arg=shlex.split(cmd)
subprocess.call(arg, cwd=parentdir)

print("Assembling pkgbuild environment.")

# make clean dirs for pkgbuild and move necessary files there:
os.system('mkdir dist/fakeroot')
os.system('mkdir dist/fakeroot/scripts')        #script "postinstall" goes here
os.system('mkdir dist/fakeroot/files')          #payload goes here
# empty scripts cause error in final install dialog, uncomment when needed:
# os.system('cp setup/postinstall-mac.sh dist/fakeroot/scripts/postinstall')
os.system('cp -r dist/DSCPlot.app dist/fakeroot/files')
print("Cleaning up PyInstaller environment.")
os.system('rm -rf dist/DSCPlot')
os.system('rm -rf dist/DSCPlot.app')

# make installer binary:
print("Bundling pkgbuild package & transferring version number.")
cmd="pkgbuild --root dist/fakeroot/files --identifier ch.ethz.dscplot.pkg --component-plist setup/dscplot.plist --version "+MainWindow.VERSION+" --install-location /Applications --scripts dist/fakeroot/scripts dist/dscplot_mac"+MainWindow.VERSION+".pkg"
arg=shlex.split(cmd)
subprocess.call(arg, cwd=parentdir)
print("Cleaning up pkgbuild environment.")
os.system('rm -rf dist/fakeroot')

print("Build finished: dscplot_mac"+MainWindow.VERSION+".pkg")