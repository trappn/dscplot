#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
run this from the main dir, e.g. Py -3 setup\make_win_release.py
this script is FOR WINDOWS ONLY !
"""
import subprocess
import os,sys,inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from dscplot.mainwindow import MainWindow as MainWindow

isspath = os.path.abspath("setup\dscplot-win.iss")  # for the windows executable

def process_iss(filepath):
    """
    update program version:
    """
    iss_file = []
    try:
        with open(filepath, 'r') as f:
            iss_file = f.read().splitlines()
    except (IOError, TypeError) as e:
        print(e)
    for num, line in enumerate(iss_file):
        if line.startswith("#define MyAppVersion"):
            l = line.split()
            l[2] = '"{}"'.format(MainWindow.VERSION)
            iss_file[num] = " ".join(l)
            break
    iss_file = [x + '\n' for x in iss_file]
    with open(filepath, 'w') as ofile:
        for line in iss_file:  
            ofile.write("%s" % line)  # write the new file

def make_win_distrib():
    innosetup_compiler = r'C:/Program Files (x86)/Inno Setup 6/ISCC.exe'
    # run setup compiler
    subprocess.call([innosetup_compiler, isspath, ])

# make PyInstaller dist:
subprocess.call(r"Py -3 -m PyInstaller run.py -y --noconsole --name DSCPlot --icon=setup/dscplot.ico --add-data dscplot/icons;icons", cwd=parentdir)

print("Updating version numbers to version {} ...".format(MainWindow.VERSION))
process_iss(isspath)
print("Version numbers updated.")
# make binary:
make_win_distrib()

print("moving installer to dist folder")
os.system('move setup\Output\* dist')
os.system('rmdir setup\Output')