from setuptools import setup

# see https://www.python.org/dev/peps/pep-0440/
# for version number specifications

setup(
    name='QTicTacToe',
    version='1.0.0',
    author='Me Myself&I',
    author_email='me@world.com',
    description='The classic game of noughts and crosses',
    url="http://qtictactoe.example.com",
    license='MIT',
    long_description=open('README.rst', 'r').read(),
    keywords='game multiplayer example pyqt5',
    project_urls={      # these are more or less arbitrary
        'Author Website': 'https://www.memyselfandi.com',
        'Publisher Website': 'https://mycompany.org',
        'Wiki': 'https://mywiki.org',
        'Source Code': 'https://git.example.com/qtictactoe'
        },
    # needed packages (=modules from our project)
    # alternative:
    # from setuptools import setup, find_package
    # packages=find_packages(),
    # this needs an __init__.py (can be empty) in every subfolder
    packages=[
        'qtictactoe',
        'qtictactoe.images'
        ],
    install_requires=['PyQt5'],
    # or e.g. ['PyQt5 >= 5.12']
    python_requires='>=3.6',
    # actually needed here because f'' strings are a >3.6 feature
    extras_require={
        'NetworkPlay': ['requests']  # this will not be automatically installed
        },
    include_package_data=True,  # this will bundle files defined in MANIFEST.in
    # alternative works only for files in module dir (qtictactoe),
    # NOTE: only one method can be used, the second definition is ignored!
    #package_data={
    #   'qtictactoe.images:[*.png]',
    #   '': ['*.txt', '*.rst']
    #   }
    # the following will be compiled to exe and bundled with the package,
    # this is optional but must point directly to callable function:
    entry_points={
        'console_scripts': ['qtictactoe = mainwindow.__main__:main'
                            ]
        }
)
